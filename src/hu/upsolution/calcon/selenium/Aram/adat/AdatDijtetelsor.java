package hu.upsolution.calcon.selenium.Aram.adat;

public class AdatDijtetelsor {

    private String tetelszam;
    private String megnevezes;
    private String leiras;
    private String megjegyzes;
    private String fejezet;
    private String mennyiseg;
    private int normaoraGep;
    private int normaoraH;

    public AdatDijtetelsor(String tetelszam, String megnevezes, String leiras, String megjegyzes, String fejezet, String mennyiseg, int normaoraGep, int normaoraH) {
        this.tetelszam = tetelszam;
        this.megnevezes = megnevezes;
        this.leiras = leiras;
        this.megjegyzes = megjegyzes;
        this.fejezet = fejezet;
        this.mennyiseg = mennyiseg;
        this.normaoraGep = normaoraGep;
        this.normaoraH = normaoraH;
    }

    public String getTetelszam() {
        return tetelszam;
    }

    public String getMegnevezes() {
        return megnevezes;
    }

    public String getLeiras() {
        return leiras;
    }

    public String getMegjegyzes() {
        return megjegyzes;
    }

    public String getFejezet() {
        return fejezet;
    }

    public String getMennyiseg() {
        return mennyiseg;
    }

    public int getNormaoraGep() {
        return normaoraGep;
    }

    public int getNormaoraH() {
        return normaoraH;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s %s %s %s %s %s", tetelszam, megnevezes, leiras, megjegyzes, fejezet, mennyiseg, normaoraGep, normaoraH);
    }
}
