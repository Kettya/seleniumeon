package hu.upsolution.calcon.selenium.Aram.adat;

public class AdatKalkulacios {

    private String nev;
    private String valasztoFelirat;
    private String valasztoDarab;
    private String numerikus;

    public AdatKalkulacios(String nev, String valasztoFelirat, String valasztoDarab, String numerikus) {
        this.nev = nev;
        this.valasztoFelirat = valasztoFelirat;
        this.valasztoDarab = valasztoDarab;
        this.numerikus = numerikus;
    }

    public String getNev() {
        return nev;
    }

    public String getValasztoFelirat() {
        return valasztoFelirat;
    }

    public String getValasztoDarab() {
        return valasztoDarab;
    }

    public String getNumerikus() {
        return numerikus;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s %s", nev, valasztoFelirat, valasztoDarab, numerikus);
    }
}
