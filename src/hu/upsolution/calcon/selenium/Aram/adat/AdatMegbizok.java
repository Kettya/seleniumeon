package hu.upsolution.calcon.selenium.Aram.adat;

public class AdatMegbizok {
    private String hosszuNev;
    private String rovidNev;
    private String varos;
    private String vallalat;

    public AdatMegbizok(String hosszuNev, String rovidNev, String varos, String vallalat) {
        this.hosszuNev = hosszuNev;
        this.rovidNev = rovidNev;
        this.varos = varos;
        this.vallalat = vallalat;
    }

    public String getHosszuNev() {
        return hosszuNev;
    }

    public String getRovidNev() {
        return rovidNev;
    }

    public String getVaros() {
        return varos;
    }

    public String getVallalat() {
        return vallalat;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s %s", hosszuNev, rovidNev, varos, vallalat);
    }
}
