package hu.upsolution.calcon.selenium.Aram.adat;

public class AdatHuman {
    private String megnevezes;
    private String  oradij;

    public AdatHuman(String megnevezes,String oradij){
        this.megnevezes=megnevezes;
        this.oradij=oradij;
    }

    public String getMegnevezes() {
        return megnevezes;
    }

    public String getOradij() {
        return oradij;
    }

    @Override
    public String toString() {
        return String.format("%s %s",megnevezes,oradij);
    }
}
