package hu.upsolution.calcon.selenium.Aram.adat;

public class AdatGep {
    private String megnevezes;
    private int oradij;

    public AdatGep(String megnevezes, int
            oradij){
        this.megnevezes=megnevezes;
        this.oradij=oradij;
    }

    public String getMegnevezes() {
        return megnevezes;
    }

    public int getOradij() {
        return oradij;
    }

    @Override
    public String toString() {
        return String.format("%s %s",megnevezes,oradij);
    }
}
