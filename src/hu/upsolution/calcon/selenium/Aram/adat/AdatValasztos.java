package hu.upsolution.calcon.selenium.Aram.adat;

public class AdatValasztos {
    private String oszlop;
    private String talaj;
    private String alaptipus;
    private String foldmunka;
    private String onhtarto;
    private String onhfejsz;
    private String cstfelfuggI;
    private String cstfelfuhhII;
    private String lengotarto;
    private String vegfeszito;
    private String fejszerknull;

    public AdatValasztos(String oszlop, String talaj, String alaptipus,
                         String foldmunka, String onhtarto, String onhfejsz,
                         String cstfelfuggI, String cstfelfuhhII, String lengotarto,
                         String vegfeszito, String fejszerknull) {
        this.oszlop = oszlop;
        this.talaj = talaj;
        this.alaptipus = alaptipus;
        this.foldmunka = foldmunka;
        this.onhtarto = onhtarto;
        this.onhfejsz = onhfejsz;
        this.cstfelfuggI = cstfelfuggI;
        this.cstfelfuhhII = cstfelfuhhII;
        this.lengotarto = lengotarto;
        this.vegfeszito = vegfeszito;
        this.fejszerknull = fejszerknull;
    }

    public String getOszlop() {
        return oszlop;
    }

    public String getTalaj() {
        return talaj;
    }

    public String getAlaptipus() {
        return alaptipus;
    }

    public String getFoldmunka() {
        return foldmunka;
    }

    public String getOnhtarto() {
        return onhtarto;
    }

    public String getOnhfejsz() {
        return onhfejsz;
    }

    public String getCstfelfuggI() {
        return cstfelfuggI;
    }

    public String getCstfelfuhhII() {
        return cstfelfuhhII;
    }

    public String getLengotarto() {
        return lengotarto;
    }

    public String getVegfeszito() {
        return vegfeszito;
    }

    public String getFejszerknull() {
        return fejszerknull;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s %s %s %s %s %s %s %s %s", oszlop, talaj, alaptipus, foldmunka, onhtarto, onhfejsz, cstfelfuggI, cstfelfuhhII, lengotarto, vegfeszito, fejszerknull);
    }
}
