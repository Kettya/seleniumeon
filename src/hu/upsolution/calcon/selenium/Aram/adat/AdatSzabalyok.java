package hu.upsolution.calcon.selenium.Aram.adat;

public class AdatSzabalyok {

    private String cikkszam;
    private String anyagNev;
    private String kovetkezmeny;
    private String szorzok1;
    private String szorzok2;
    private String szorzok3;
    private String ar;
    private String tipus1;
    private String tipus2;
    private String tipus3;

    public AdatSzabalyok(String cikkszam, String anyagNev, String kovetkezmeny, String szorzok1, String szorzok2, String szorzok3, String ar, String tipus1, String tipus2, String tipus3) {
        this.cikkszam = cikkszam;
        this.anyagNev = anyagNev;
        this.kovetkezmeny = kovetkezmeny;
        this.szorzok1 = szorzok1;
        this.szorzok2 = szorzok2;
        this.szorzok3 = szorzok3;
        this.ar = ar;
        this.tipus1 = tipus1;
        this.tipus2 = tipus2;
        this.tipus3 = tipus3;

    }

    public String getCikkszam() {
        return cikkszam;
    }

    public String getAnyagNev() {
        return anyagNev;
    }

    public String getKovetkezmeny() {
        return kovetkezmeny;
    }

    public String getSzorzok1() {
        return szorzok1;
    }

    public String getSzorzok2() {
        return szorzok2;
    }

    public String getSzorzok3() {
        return szorzok3;
    }

    public String getAr() {
        return ar;
    }

    public String getTipus1() {
        return tipus1;
    }

    public String getTipus2() {
        return tipus2;
    }

    public String getTipus3() {
        return tipus3;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s %s %s %s %s %s %s %s", cikkszam, anyagNev, kovetkezmeny, szorzok1, szorzok2, szorzok3, ar, tipus1, tipus2, tipus3);
    }
}
