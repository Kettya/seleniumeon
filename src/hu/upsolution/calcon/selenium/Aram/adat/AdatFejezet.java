package hu.upsolution.calcon.selenium.Aram.adat;

public class AdatFejezet {
    private String sorszam;
    private String megnevezes;
    private String rovidNev;
    private String humanSzorzo;
    private String gepSzorzo;

    public AdatFejezet(String sorszam, String megnevezes, String rovidNev, String humanSzorzo, String gepSzorzo) {
        this.sorszam = sorszam;
        this.megnevezes = megnevezes;
        this.rovidNev = rovidNev;
        this.humanSzorzo = humanSzorzo;
        this.gepSzorzo = gepSzorzo;
    }

    public String getSorszam() {
        return sorszam;
    }

    public String getMegnevezes() {
        return megnevezes;
    }

    public String getRovidNev() {
        return rovidNev;
    }

    public String getGepSzorzo() { return gepSzorzo;
    }

    public String getHumanSzorzo() { return humanSzorzo; }

    public void setHumanSzorzo(String humanSzorzo) {
        this.humanSzorzo = humanSzorzo;
    }

    public void setGepSzorzo(String gepSzorzo) {
        this.gepSzorzo = gepSzorzo;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s %s %s", sorszam, megnevezes, rovidNev, humanSzorzo, gepSzorzo);
    }
}
