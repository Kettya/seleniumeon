package hu.upsolution.calcon.selenium.Aram;

import hu.upsolution.calcon.selenium.Aram.adat.AdatDijtetelsor;
import hu.upsolution.calcon.selenium.Aram.adat.AdatFejezet;
import hu.upsolution.calcon.selenium.Aram.adat.AdatGep;
import hu.upsolution.calcon.selenium.Aram.adat.AdatHuman;
import hu.upsolution.calcon.selenium.Basic;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.*;

public class Dijtetelsor extends Basic {
    private String keresett;
    private String keresettAr;
    private String dijtetelsorAllapot;
    private String invalid = "";
    private String invalidSmallText = "";
    private String invalidLongText = "";
    private String invalidSmallString = "";
    private String zero = "";
    private boolean isCsere = false;
    private boolean isTorles = false;

    private List<AdatFejezet> adatFejezets = new ArrayList<>();
    private List<AdatHuman> adatHumen = new ArrayList<>();
    private List<AdatGep> adatGeps = new ArrayList<>();
    private List<AdatDijtetelsor> adatDijtetelsors = new ArrayList<>();
    private List<String> cserelt = new ArrayList<>();
    private HashMap<String, String> csereltEroforras = new HashMap<>();
    private List<String> torolt = new ArrayList<>();

    @BeforeTest
    public void SetUp() throws FileNotFoundException {
        Scanner scFejezet = new Scanner(new File("adat/fejezetadat.txt"));
        Scanner scHuman = new Scanner(new File("adat/humanadat.txt"));
        Scanner scGep = new Scanner(new File("adat/gepadat.txt"));
        Scanner scDijtetel = new Scanner(new File("adat/dijteteladatok.csv"));

        while (scFejezet.hasNextLine()) {
            String[] split = scFejezet.nextLine().split(";");
            adatFejezets.add(new AdatFejezet(split[0], split[1], split[2], split[3], split[4]));
        }
        scFejezet.close();

        while (scHuman.hasNextLine()) {
            String[] split = scHuman.nextLine().split(";");
            adatHumen.add(new AdatHuman(split[0], split[1]));
        }
        scHuman.close();

        while (scGep.hasNextLine()) {
            String[] split = scGep.nextLine().split(";");
            adatGeps.add(new AdatGep(split[0], Integer.parseInt(split[1])));
        }
        scGep.close();

        while (scDijtetel.hasNextLine()) {
            String[] split = scDijtetel.nextLine().split(";");
            adatDijtetelsors.add(new AdatDijtetelsor(split[0], split[1], split[2], split[3], split[4], split[5], Integer.parseInt(split[6]), Integer.parseInt(split[7])));
            //abcd.add(new AdatDijtetelsor(split[0], split[1], split[2], split[3], Integer.parseInt(split[4]), Integer.parseInt(split[5])));
        }
        scDijtetel.close();


    }

    private void Wait() {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
    }

    private void PontMenu() {
        Wait();
        List<WebElement> pontMenu = driver.findElements(By.cssSelector("m-subheader .m-dropdown"));
        if (pontMenu.size() > 0) {
            System.out.println(" ... Menü ");
            Assert.fail();
        }
    }

    private void Zero() {
        Random random = new Random();
        int rndZero = random.nextInt(255) + 20;
        for (int i = 0; i < rndZero; i++) {
            zero += "0";
        }
    }

    private void InvalidString() {
        Random random = new Random();
        String abc = "aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyz 0123456789";
        int rndBetu = random.nextInt(15) + 255;
        for (int i = 0; i < rndBetu; i++) {
            char c = abc.charAt(random.nextInt(abc.length()));
            invalid += c;
        }

    } //invalid String

    private void InvalidSmallString() {
        Random random = new Random();
        String abc = "aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyz 0123456789";
        int rndBetu = random.nextInt(15) + 16;
        for (int i = 0; i < rndBetu; i++) {
            char c = abc.charAt(random.nextInt(abc.length()));
            invalidSmallString += c;
        }

    }

    public void InvalidSmallText() {
        Random random = new Random();
        String abc = "aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyz 0123456789";
        int rndBetu = random.nextInt(15) + 1000;
        for (int i = 0; i < rndBetu; i++) {
            char c = abc.charAt(random.nextInt(abc.length()));
            invalidSmallText += c;
        }
    } //invalid Small Text

    public void InvalidLongText() {
        Random random = new Random();
        String abc = "aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyz 0123456789";
        int rndBetu = random.nextInt(15) + 2000;
        for (int i = 0; i < rndBetu; i++) {
            char c = abc.charAt(random.nextInt(abc.length()));
            invalidLongText += c;
        }
    }

    void DijtetelsorValasztasAktiv() {
        driver.findElement(By.cssSelector("m-dijtetelsor-valaszto .mat-select")).click();
        List<WebElement> dijtetelsorValaszto = driver.findElements(By.cssSelector(".mat-option"));

        for (WebElement element : dijtetelsorValaszto) {
            if (element.getText().contains("Aktív")) {
                dijtetelsorAllapot = "Aktív";
                element.click();
                break;
            }
        }
    }

    void DijtetelsorValasztasIdozitett() {
        driver.findElement(By.cssSelector("m-dijtetelsor-valaszto .mat-select")).click();
        List<WebElement> dijtetelsorValaszto = driver.findElements(By.cssSelector(".mat-option"));

        for (WebElement element : dijtetelsorValaszto) {
            if (element.getText().contains("Időzített")) {
                dijtetelsorAllapot = "Időzített";
                element.click();
                break;
            }
        }

    }

    void FejezetMain() {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        WebElement element = driver.findElement(By.cssSelector("a[href*=fejezetek]"));
        element.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        Assert.assertTrue(driver.getCurrentUrl().contains("fejezetek"));

    }

    void FejezetHozzaadasMain() {
        PontMenu();
        //     WebElement ujFejezet = driver.findElement(By.partialLinkText("Új fejezet"));
        WebElement ujFejezet = driver.findElement(By.cssSelector("m-subheader a.btn"));
        //wait.until(ExpectedConditions.elementToBeClickable(ujFejezet));

        List<WebElement> fejezetek = driver.findElements(By.cssSelector("mat-row"));
        String expFejezetek = "https://calcon.upsolution.hu/fejezetek";
        // Assert.assertEquals(driver.getCurrentUrl(), expFejezetek);

        logger.info(driver.getCurrentUrl());
        ujFejezet.click();
        logger.info("Klikk az Új fejezet hozzáadása gombra");

        // Thread.sleep(2000);
        WebElement mentes = driver.findElement(By.cssSelector(".m-form button"));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        //  wait.until(ExpectedConditions.elementToBeClickable(fejezet));

        String expUjFejezet = "10.10.1.25:91/fejezetek/uj";
        //   Assert.assertEquals(driver.getCurrentUrl(), expUjFejezet);

        logger.info(driver.getCurrentUrl());
        //    Thread.sleep(2000);

        // int randomsorszam = random.nextInt(99) + 1;
        int randomsorszam = fejezetek.size() + 1;
        //  String sorszam = null;
        String megnevezes = null;
        String rovid;
        String human;
        String gep;

        WebElement sorsz = driver.findElement(By.name("sorszam"));
        WebElement megnev = driver.findElement(By.name("megnevezes"));
        WebElement rovidn = driver.findElement(By.name("rovidnev"));
        WebElement humansz = driver.findElement(By.name("humanSzorzo"));
        WebElement gepsz = driver.findElement(By.name("gepSzorzo"));

        try {
            int rnd = 12;

//            for (int i = 0; i < fejezetek.size(); i++) {
//                while (adatFejezets.get(rnd).getMegnevezes().equals(fejezetek.get(i).getText())) {
//                    rnd = random.nextInt(adatFejezets.size());
//                }
//            }
            //  sorszam = adatFejezets.get(rnd).getSorszam();
            megnevezes = adatFejezets.get(rnd).getMegnevezes();
            rovid = adatFejezets.get(rnd).getRovidNev();
            human = adatFejezets.get(rnd).getHumanSzorzo();
            gep = adatFejezets.get(rnd).getGepSzorzo();

            //sorsz.sendKeys(sorszam);
            sorsz.sendKeys(String.valueOf(randomsorszam));
            megnev.sendKeys(megnevezes);
            rovidn.sendKeys(rovid);
            humansz.clear();
            humansz.sendKeys(human);
            gepsz.clear();
            gepsz.sendKeys(gep);

            logger.info("Új fejezet hozzáadása a következő adatokkal:");
            logger.info("Sorszám: " + randomsorszam);
            logger.info("Megnevezés: " + megnevezes);
            logger.info("Rövid megnevezés: " + rovid);
            logger.info("Humán szorzó: " + human);
            logger.info("Gép szorzó: " + gep);

            mentes.click();
            logger.info("Klikk a mentés gombra");

            // wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".m-form button")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

            if (driver.getCurrentUrl().equals(expUjFejezet)) {
                if (driver.findElement(By.cssSelector(".mat-dialog-container")).isDisplayed()) {
                    logger.error(driver.findElement(By.cssSelector(".hiba-dialog__szoveg")).getText());
                    driver.findElement(By.cssSelector(".mat-dialog-container button")).click();
                    logger.error("Something wrong");
                    Assert.fail();
                } else {
                    logger.error("Helytelen adatok");
                }

            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

        //  Assert.assertEquals(fDriver.getCurrentUrl(), expFejezetek);

        if (driver.getCurrentUrl().equals(expFejezetek)) {

            try {

                driver.findElement(By.name("sorszam")).sendKeys(String.valueOf(randomsorszam));
                driver.findElement(By.name("megnevezes")).sendKeys(megnevezes);
                driver.findElement(By.name("megnevezes")).sendKeys(Keys.ENTER);

            } catch (Exception e) {

                logger.info(e.getMessage());
            }


            Wait();

            if (driver.findElement(By.cssSelector(".mat-cell a")).isDisplayed()) {

                logger.info("A fejezet hozzáadása sikeresen megtörtént");

            } else logger.info("Something wrong");
        }
    }

    void FejezetHozzaadasInvalidMain() {
        //  PontMenu();
        Random random = new Random();
        driver.findElement(By.cssSelector("m-subheader a.btn")).click();

        Wait();

        String sorszam;
        String sorszamAfter;
        WebElement sorsz = driver.findElement(By.name("sorszam"));
        WebElement megnev = driver.findElement(By.name("megnevezes"));
        WebElement rovidn = driver.findElement(By.name("rovidnev"));
        WebElement humansz = driver.findElement(By.name("humanSzorzo"));
        WebElement gepsz = driver.findElement(By.name("gepSzorzo"));

        Zero();
        InvalidString();
        InvalidSmallString();
        System.out.println(zero.length() + " | " + invalid.length() + " | " + invalidSmallString.length());
        sorszam = zero + String.valueOf(random.nextInt(2147483647) + 1111111111);
        sorsz.sendKeys(sorszam);
        megnev.sendKeys(invalid);
        rovidn.sendKeys(invalidSmallString);
        sorszamAfter = sorsz.getAttribute("value");
        Assert.assertEquals(sorszamAfter, sorszam);
        if (Long.parseLong(sorszam) > 2147483647) {
            Assert.assertTrue(driver.findElement(By.cssSelector("mat-error")).isDisplayed());
        }
        System.out.println(sorszam);
        System.out.println(sorszamAfter);

        driver.findElement(By.cssSelector("m-uj-fejezet button.btn-primary")).click();
    }

    void DijtetelekMain() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        Wait();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        driver.findElement(By.cssSelector("a[href*=dijtetelek]")).click();
    }

    void PdfMain() {
        WebElement elementPdf = driver.findElement(By.cssSelector("a.btn:nth-child(2)"));

        try {

            logger.info("Díjtételsor pdf exportálása");

            wait.until(ExpectedConditions.elementToBeClickable(elementPdf));
            elementPdf.click();

            Robot robot = new Robot();

            Thread.sleep(2000);
            robot.keyPress(KeyEvent.VK_DOWN);
            // robot.keyRelease(KeyEvent.VK_DOWN);

            // Press tab key three time to navigate to Save button.
            for (int i = 0; i < 3; i++) {
                Thread.sleep(2000);
                robot.keyPress(KeyEvent.VK_TAB);
            }

            // Press down Save button.
            Thread.sleep(2000);
            robot.keyPress(KeyEvent.VK_ENTER);

            logger.info("A pdf megnyitása sikeresen megtörtént");

        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    void FejezetKeresesMain() {
        Random random = new Random();
        //region Fejezet keresése
        try {

            int searchsucces = 1;
            while (searchsucces == 1) {
                int search = random.nextInt(3) + 1;
                int character = random.nextInt(2) + 1;
                int rndFejezetekList = random.nextInt(adatFejezets.size());
                String alphabet = "abcdefghijklmnopqrstuvwxyz";
                String s = "";
                char c = alphabet.charAt(random.nextInt(26));

                WebElement kereses = driver.findElement(By.cssSelector(".ng-star-inserted .form-group button"));
                WebElement megnevezes = driver.findElement(By.name("megnevezes"));
                WebElement sorszam = driver.findElement(By.name("sorszam"));

                megnevezes.clear();
                sorszam.clear();

                if (search == 1) {
                    int k = random.nextInt(20);
                    System.out.println(k);
                    sorszam.sendKeys(String.valueOf(k));

                }
                if (search == 2) {
                    String word = adatFejezets.get(rndFejezetekList).getMegnevezes().trim();
                    megnevezes.sendKeys(word);
                    System.out.println(word);

                }
                if (search == 3) {
                    System.out.println(c + " letter");
                    megnevezes.sendKeys(String.valueOf(c));
                }

                Wait();
                kereses.click();
                Thread.sleep(2500);
                Wait();
                List<WebElement> results = driver.findElements(By.cssSelector("mat-cell .btn.btn-primary"));

                if (results.size() > 0) {
                    searchsucces = 2;
                    results.get(random.nextInt(results.size())).click();
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        //endregion
    } //main

    void DijtetelHozzaadasMain() {
        Random random = new Random();
        //region Új díjtételsor gomb
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

        try {
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
            driver.findElement(By.cssSelector("m-subheader a.btn")).click();

        } catch (Exception e) {

            logger.error("Something wrong  " + e.getMessage());
        }


        Assert.assertTrue(driver.getCurrentUrl().contains("uj"));

        if (driver.getCurrentUrl().contains("uj")) {

            //logger.info(ujDijtetelUrl);
            logger.info("Új díjtétel hozzáadása");

        } else {

            logger.error("Something wrong");
        }
        //endregion

        WebElement tetelSzam = driver.findElement(By.name("tetelszam"));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.name("tetelszam")));

        String tetelsz;
        String megnevezes = null;
        String leiras;
        String megjegyzes;

        int normagep;
       // int normahuman;
        // int randomtetel = 0;
        double gepszorzo = 0;
        double humnaszorzo = 0;


        try {

            while (driver.getCurrentUrl().contains("uj")) {

                //region Tételszám, megnevezés, leírás, megjegyzés, mértékegység megadása
                int rndDij = 3;
                // randomtetel = adatDijtetelsors.get(rndDij).getTetelszam();

                tetelsz = adatDijtetelsors.get(rndDij).getTetelszam();
                megnevezes = adatDijtetelsors.get(rndDij).getMegnevezes();
                leiras = adatDijtetelsors.get(rndDij).getLeiras();
                megjegyzes = adatDijtetelsors.get(rndDij).getMegjegyzes();

                String asa = tetelsz;
                System.out.println(megnevezes);
                //tetelSzam.clear();
                System.out.println(asa.length());
               // String[] numb = asa.split("");
                tetelSzam.sendKeys(asa);
                //  tetelSzam.sendKeys(numb[numb.length - 1]);
//                for (int i = 0; i <= numb.length; i++) {
//                  //  tetelSzam.sendKeys(n);
//                    System.out.println(numb[i]);
//                }


                logger.info("Tételszám: " + tetelsz);
                driver.findElement(By.cssSelector("input[name=fejezet]")).click();

                wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
                java.util.List<WebElement> fejezet = driver.findElements(By.cssSelector("mat-option"));

                String currentFejezet = adatDijtetelsors.get(rndDij).getFejezet();
                System.out.println(fejezet.size());

                for (WebElement element : fejezet) {
                    if (element.getText().contains(currentFejezet)) {
                        element.click();
                        break;
                    }
                }

                for (AdatFejezet adatFejezet : adatFejezets) {
                    if (adatFejezet.getRovidNev().contains(currentFejezet)) {
                        gepszorzo = Double.parseDouble(adatFejezet.getGepSzorzo());
                        humnaszorzo = Double.parseDouble(adatFejezet.getHumanSzorzo());

                    }
                }
                logger.info("Fejezet: " + currentFejezet);
                logger.info("A fejezethez megadott humánerőforrásszorzó: " + humnaszorzo);
                logger.info("A fejezethez megadott gépszorzó: " + gepszorzo);

                WebElement elementMegnevezes = driver.findElement(By.name("megnevezes"));
                WebElement elementLeiras = driver.findElement(By.name("leiras"));
                WebElement elementMegjegyzes = driver.findElement(By.name("megjegyzes"));
                WebElement elementMertekegyseg = driver.findElement(By.cssSelector("mat-select"));

                elementMegnevezes.sendKeys(megnevezes);
                elementLeiras.sendKeys(leiras);
                elementMegjegyzes.sendKeys(megjegyzes);
                elementMertekegyseg.click();

                logger.info("Megnevezés: " + megnevezes);
                logger.info("Leírás: " + leiras);
                logger.info("Megjegyzés: " + megjegyzes);

                wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

                java.util.List<WebElement> mennyiseg = driver.findElements(By.tagName("mat-option"));
                int rndMe = random.nextInt(mennyiseg.size());
                String currentMennyiseg = adatDijtetelsors.get(rndDij).getMennyiseg();
                wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
                for (WebElement element : mennyiseg) {
                    if (currentMennyiseg.equals(" ") || currentMennyiseg.equals("m3")) {
                        currentMennyiseg = mennyiseg.get(rndMe).getText();
                        mennyiseg.get(rndMe).click();
                        break;
                    }
                    if (element.getText().toLowerCase().contains(currentMennyiseg)) {
                        currentMennyiseg = element.getText();
                        element.click();
                        break;
                    }
                }

                logger.info("A kiválasztott mértékegység: " + currentMennyiseg);
                //endregion
                wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
                //region Gép hozzáadása
                int gepmennyiseg = random.nextInt(5) + 1;
                int osszGep = 0;
                int gepDij = 0;
                int osszesen = 0;
                int osszesenGep = 0;

                WebElement megjegyzesElement = driver.findElement(By.name("megjegyzes"));
                WebElement gepkoltsegBtn = driver.findElement(By.partialLinkText("Gépköltség"));

                for (int a = 0; a < gepmennyiseg; a++) {

                    normagep = adatDijtetelsors.get(rndDij).getNormaoraGep();

                    ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", megjegyzesElement);
                    wait.until(ExpectedConditions.elementToBeClickable(gepkoltsegBtn));

                    gepkoltsegBtn.click();
                    driver.findElement(By.cssSelector(".ng-star-inserted:nth-of-type(" + (a + 1) + ") [name=gepId]")).click();

                    java.util.List<WebElement> gep = driver.findElements(By.className("mat-option"));
                    int rndGep = random.nextInt(gep.size());
                    String gepSz = gep.get(rndGep).getText();

                    wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.className("mat-option"))));
                    logger.info("Kiválasztott munkagép: " + gep.get(rndGep).getText());
                    logger.info("A géphez tartozó normaóra: " + normagep);

                    for (AdatGep adatGep : adatGeps) {

                        if (adatGep.getMegnevezes().equals(gepSz)) {
                            gepDij = adatGep.getOradij();
                            logger.info(gepDij);
                            break;
                        }
                        //logger.info(gepDij);
                    }

                    logger.info("HUF " + String.format("%,d", (normagep * gepDij)));
                    driver.findElement(By.cssSelector("m-gep-ora.ng-star-inserted:nth-of-type(" + (a + 1) + ") [name=oraSzam]")).sendKeys(String.valueOf(normagep));

                    osszGep = normagep * gepDij;
                    gep.get(rndGep).click();
                    osszesenGep += osszGep;

                }
                //endregion

                //region Humánerőforrás hozzáadása
                int humanmennyiseg = random.nextInt(2);
                int humanDij = 0;
                int osszHuman;
                int osszesenHuman = 0;

                WebElement humankoltsegBtn = driver.findElement(By.partialLinkText("Human erőforrás"));
                //System.out.println(humanmennyiseg);

                for (int a = 0; a <= humanmennyiseg; a++) {

                    normagep = adatDijtetelsors.get(rndDij).getNormaoraH();

                    ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", gepkoltsegBtn);
                    wait.until(ExpectedConditions.elementToBeClickable(humankoltsegBtn));
                    humankoltsegBtn.click();
                    driver.findElement(By.cssSelector(".ng-star-inserted:nth-of-type(" + (a + 1) + ") [name=humanId]")).click();

                    List<WebElement> human = driver.findElements(By.className("mat-option"));
                    int rndHuman = random.nextInt(human.size());

                    wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.className("mat-option"))));
                    //System.out.println(human.get(rndHuman).getText());
                    logger.info("Kiválasztott humánerőforrás: " + human.get(rndHuman).getText());
                    logger.info("A humánerőforráshoz tartozó normaóra: " + normagep);

                    for (int i = 1; i < adatHumen.size(); i++) {

                        if (human.get(rndHuman).getText().equals(adatHumen.get(i).getMegnevezes())) {
                            humanDij = Integer.parseInt(adatHumen.get(i).getOradij());
                            logger.info(humanDij);
                            break;
                        }
                    }
                    logger.info(humanDij);
                    driver.findElement(By.cssSelector("m-human-ora.ng-star-inserted:nth-of-type(" + (a + 1) + ") [name=oraSzam]")).sendKeys(String.valueOf(normagep));
                    human.get(rndHuman).click();

                    osszHuman = normagep * humanDij;
                    osszesenHuman += osszHuman;

                }
                //endregion

//                //region Összegellenőrzés
                //
                String eredmeny = driver.findElement(By.cssSelector(".m-form h6")).getText();
                String[] split = eredmeny.split(" ");
                String b = split[1].replaceAll(",", "");

                double doubleGep = osszesenGep;
                double doubleGepszorzat = doubleGep * gepszorzo;
                double doublehuman = osszesenHuman;
                double doubleHumanszorzat = doublehuman * humnaszorzo;
                double doubleOsszesen = doubleGepszorzat + doubleHumanszorzat;

                int tenylegesOsszeg = (int) doubleOsszesen;
                int tenyleges2 = tenylegesOsszeg + 1;
                int round = (int) Math.round(doubleOsszesen);
                int ceil = (int) Math.ceil(doubleOsszesen);
                int koltseg = Integer.parseInt(b);
                int kulonbseg = koltseg - tenylegesOsszeg;

                logger.info("Tényleges költség: " + String.format("%,d", tenylegesOsszeg) + " +/- 1");
                logger.info("Double összeg:" + doubleOsszesen);
                logger.info("Kerekített összeg (Math.round): " + round);
                logger.info("Kerekített összeg (Math.ceil): " + ceil);
                logger.info("Elvárt költség: " + String.format("%,d", koltseg));
//
                Assert.assertTrue(kulonbseg <= 1);
//
//                //endregion

                //region Mentés, adatellenőrzés

                JavascriptExecutor javascript = (JavascriptExecutor) driver;
                javascript.executeScript("window.scrollTo(0, document.body.scrollHeight)", "");

                ///html/body/m-pages/div/div/div/m-dijtetel/div/div/form/div[12]/button
                WebElement elemenMentes = driver.findElement(By.cssSelector("m-dijtetel button.btn-primary"));
                wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

                if (elemenMentes.isEnabled())
                    elemenMentes.click();

                if ((humanmennyiseg == 0 && gepmennyiseg == 0)) {
                    wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector(".hiba-dialog__uzenetek"))));
                    driver.findElement(By.cssSelector("/m"));
                }

                wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

                if (driver.getCurrentUrl().contains("uj")) {

                    elementMegnevezes.clear();
                    elementLeiras.clear();
                    elementMegjegyzes.clear();
                }
                //endregion
            }

        } catch (Exception e) {

            e.printStackTrace();
            logger.error(e.getMessage());
        }
        wait.until(ExpectedConditions.presenceOfElementLocated(By.tagName("m-brand")));
        // Assert.assertEquals(driver.getCurrentUrl(), "https://calcon.upsolution.hu/dijtetelek");
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        if (driver.getCurrentUrl().equals("https://calcon.upsolution.hu/dijtetelek")) {
            try {
                // driver.findElement(By.name("tetelszam")).sendKeys(String.valueOf(tetelsz), Keys.ENTER);
                driver.findElement(By.name("megnevezes")).sendKeys(megnevezes);

            } catch (Exception e) {
                logger.info(e.getMessage());
            }

            //Thread.sleep(500);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("mat-cell")));

            if (driver.findElement(By.tagName("mat-cell")).isDisplayed()) {
                logger.info("A díjtétel hozzáadása sikeresen megtörtént");

            } else logger.info("Something wrong");
        }

    }

    void GepekMain() {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        driver.findElement(By.cssSelector("li a[href*=gepek]")).click();
    }

    void GepHozzaadasMain() {
        PontMenu();
        WebElement ujGep = driver.findElement(By.cssSelector("a[href*=uj]"));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        //wait.until(ExpectedConditions.elementToBeClickable(ujGep));

        try {

            //wait.until(ExpectedConditions.elementToBeClickable(ujGep));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
            ujGep.click();

            logger.info("Klikk az Új gép hozzáadása gombra");

        } catch (Exception e) {

            logger.error(e.getMessage());
        }

        logger.info(driver.getCurrentUrl());
        //region Új gép hozzáadása

        try {

            while (driver.getCurrentUrl().contains("uj")) {
                String megnevezes;
                int oradij = 0;
                int rnd = 7;

                WebElement megN = driver.findElement(By.cssSelector("input[name=nev]"));
                WebElement oraD = driver.findElement(By.cssSelector("input[name=oradij]"));

                WebElement mentes = driver.findElement(By.cssSelector(".m-content button.btn-primary"));
                //System.out.println( mentes.getAttribute("disabled"));

                megnevezes = adatGeps.get(rnd).getMegnevezes();
                oradij = adatGeps.get(rnd).getOradij();

                megN.sendKeys(megnevezes);
                logger.info(megnevezes);

                oraD.sendKeys(String.valueOf(oradij));
                logger.info(oradij);


                mentes.click();
                if (driver.getCurrentUrl().contains("uj")) {
                    megN.clear();
                    oraD.clear();

                }

                if (oradij > 999999) {
                    //mentes.click();
                    WebElement rendben = driver.findElement(By.cssSelector(".cdk-overlay-pane  button"));
                    Assert.assertTrue(rendben.isDisplayed());
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".cdk-overlay-pane")));
                    wait.until(ExpectedConditions.visibilityOf(rendben));
                    rendben.click();
                    logger.error("Helytelen adatok! Az óradíj nem lehet nagyobb, mint 999999!!");
                }

            }
        } catch (Exception e) {

            logger.error(e.getMessage());
        }
        //endregion

        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".m-menu__item a[href*=gepek]")));
        logger.info(driver.getCurrentUrl());
        Assert.assertTrue(driver.getCurrentUrl().contains("gepek"));

    }

    void GepKeresesMain() {
        Random random = new Random();
        WebElement megnevezes = driver.findElement(By.cssSelector(".form-group [name=megnevezes]"));
        WebElement kereses = driver.findElement(By.cssSelector(".form-group button"));
        String szerkesztesMegnevezes;
        String szerkesztesAr;

        String alphabet = "aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyz";
        String s = "";
        int a = 1;

        try {
            wait.until(ExpectedConditions.visibilityOf(megnevezes));
            while (a == 1) {
                char c = alphabet.charAt(random.nextInt(35));
                megnevezes.sendKeys(String.valueOf(c));
                wait.until(ExpectedConditions.elementToBeClickable(kereses));
                kereses.click();
//                Thread.sleep(2000);
                List<WebElement> eredmenyekMegnevezes = driver.findElements(By.cssSelector(".mat-cell:nth-of-type(1)"));
                List<WebElement> eredmenyekAr = driver.findElements(By.cssSelector(".mat-cell:nth-of-type(2)"));
                List<WebElement> eredmenyek = driver.findElements(By.cssSelector(".mat-cell a"));

//                eredmenyekMegnevezes.forEach(System.out::println);


                if (eredmenyek.size() == 0) {
                    megnevezes.clear();
                } else {
                    Thread.sleep(2000);
                    a = 2;
                    int rnd = random.nextInt(eredmenyek.size());
                    keresett = eredmenyekMegnevezes.get(rnd).getText();
                    keresettAr = eredmenyekAr.get(rnd).getText().toLowerCase().replace("ft/óra", "").trim();
                    //keresettGepAr.replace("Ft/óra","");

                    eredmenyek.get(rnd).click();
//                    String asa = eredmenyekMegnevezes.get(rnd).getText();
//                    System.out.println(asa);
                    System.out.println(keresett);
                }

            }
        } catch (Exception e) {
            logger.info(e.getMessage());
        }
        Wait();
        szerkesztesMegnevezes = driver.findElement(By.cssSelector("input[name=megnevezes]")).getAttribute("value");
        szerkesztesAr = driver.findElement(By.cssSelector("input[name=oradij]")).getAttribute("value");
        System.out.println(keresettAr);
        System.out.println(szerkesztesAr);

        Assert.assertTrue(driver.getCurrentUrl().contains("/gepek/"));
        Assert.assertEquals(driver.findElement(By.cssSelector(".m-subheader__title")).getText(), "Gép szerkesztése");
//        Assert.assertTrue(keresettGep.equals(szerkesztesMegnevezes));
//        Assert.assertTrue(keresettGepAr.contains(szerkesztesAr));
    }

    void HumanMain() {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        driver.findElement(By.cssSelector("a[href*=humaneroforras]")).click();
    }

    void HumanHozzaadasMain() {
        PontMenu();
        //Random random = new Random();
        WebElement ujHuman = driver.findElement(By.cssSelector("a[href*=uj]"));
        wait.until(ExpectedConditions.elementToBeClickable(ujHuman));

        try {

            wait.until(ExpectedConditions.elementToBeClickable(ujHuman));
            ujHuman.click();
            logger.info("Klikk az Új erőforrás hozzáadása gombra");

        } catch (Exception e) {

            logger.error(e.getMessage());
        }

        logger.info(driver.getCurrentUrl());

        String megnevezes;
        int oradij;

        WebElement megN = driver.findElement(By.name("megnevezes"));
        WebElement oraD = driver.findElement(By.name("oradij"));

        try {

            while (driver.getCurrentUrl().contains("uj")) {

                int rnd = 4;

                megnevezes = adatHumen.get(rnd).getMegnevezes();
                oradij = Integer.parseInt(adatHumen.get(rnd).getOradij());

                megN.sendKeys(megnevezes);
                oraD.sendKeys(String.valueOf(oradij));
                //    Thread.sleep(2000);
                logger.info(megnevezes);
                logger.info(oradij);

                WebElement mentes = driver.findElement(By.cssSelector("m-uj-humaneroforras button"));
                mentes.click();

            }

        } catch (Exception e) {

            logger.error(e.getMessage());
        }

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

        logger.info(driver.getCurrentUrl());
        Assert.assertEquals(driver.getCurrentUrl(), "http://10.10.1.25:91/humaneroforras");
    }

    void HumanKeresesMain() {
        Random random = new Random();
        String szerkesztesMegnevezes;
        String szerkesztesAr;
        try {

            String alphabet = "aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyz";
            String s = "";
            int a = 1;

            while (a == 1) {
                char c = alphabet.charAt(random.nextInt(35));

                WebElement megnevezes = driver.findElement(By.cssSelector(".form-group [name=megnevezes]"));
                WebElement kereses = driver.findElement(By.cssSelector(".form-group button"));

                megnevezes.sendKeys(String.valueOf(c));
                wait.until(ExpectedConditions.elementToBeClickable(kereses));
                kereses.click();

                List<WebElement> eredmenyekMegnevezes = driver.findElements(By.cssSelector(".mat-cell:nth-of-type(1)"));
                List<WebElement> eredmenyekAr = driver.findElements(By.cssSelector(".mat-cell:nth-of-type(2)"));
                List<WebElement> eredmenyek = driver.findElements(By.cssSelector(".mat-cell a"));
                if (eredmenyek.size() == 0) {
                    megnevezes.clear();
                } else {
                    a = 2;
                    int rnd = random.nextInt(eredmenyek.size());
                    keresett = eredmenyekMegnevezes.get(rnd).getText();
                    keresettAr = eredmenyekAr.get(rnd).getText().toLowerCase().replace("ft/óra", "").trim();
                    eredmenyek.get(rnd).click();
                }

            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        Wait();
        szerkesztesMegnevezes = driver.findElement(By.cssSelector("input[name=megnevezes]")).getAttribute("value");
        szerkesztesAr = driver.findElement(By.cssSelector("input[name=oradij]")).getAttribute("value");
        System.out.println(keresettAr);
        System.out.println(szerkesztesAr);

        Assert.assertTrue(driver.getCurrentUrl().contains("/humaneroforras/"));
        Assert.assertEquals(driver.findElement(By.cssSelector(".m-subheader__title")).getText(), "Erőforrás szerkesztés");
    }

    void DijtetelKeresesMain() {
        Random random = new Random();
        try {
            String alphabet = "aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyz";
            int searchsuccess = 1;

            WebElement elementMegnevezes = driver.findElement(By.cssSelector(".form-group [name=megnevezes]"));
            WebElement elementTetelszam = driver.findElement(By.cssSelector(".form-group [name=tetelszam]"));
            WebElement elementKereses = driver.findElement(By.cssSelector(".form-group button"));

            while (searchsuccess == 1) {
                char c = (char) alphabet.charAt(random.nextInt(35));
                String megnevezesFromList = adatDijtetelsors.get(random.nextInt(adatDijtetelsors.size())).getMegnevezes();
                System.out.println(c);
                System.out.println(megnevezesFromList);
                elementMegnevezes.clear();
                elementTetelszam.clear();

                // elementMegnevezes.sendKeys(String.valueOf(c), Keys.ENTER);
                elementMegnevezes.sendKeys(String.valueOf(c));
                Thread.sleep(200);
                elementKereses.click();

                List<WebElement> eredmenyek = driver.findElements(By.cssSelector("mat-cell a"));

                if (eredmenyek.size() >= 1) {

                    searchsuccess = 2;
                    eredmenyek.get(random.nextInt(eredmenyek.size())).click();
                }
            }

        } catch (Exception e) {
            logger.info(e.getMessage());
        }
    }

    void InaktivitasMain() {
        PontMenu();
        Random random = new Random();
        List<WebElement> dijtetelsorSzam = driver.findElements(By.cssSelector("mat-cell:nth-of-type(2)"));
        List<WebElement> muveletek = driver.findElements(By.cssSelector("mat-cell mat-select"));
        List<WebElement> gepekList;
        int rndGepek;
        int csere = 0;
        int torles = 0;
        String s = "";
        // int rndM = random.nextInt(muveletek.size());
        //  muveletek.get(rndM).click();
        System.out.println(keresett);
        if (muveletek.size() > 0) {
            for (int i = 0; i < muveletek.size(); i++) {
                int rndOption = random.nextInt(2) + 2;
                muveletek.get(i).click();
                if (rndOption == 2 && (!driver.findElement(By.cssSelector("mat-option:nth-of-type(" + (rndOption) + ")")).isEnabled())) {
                    rndOption = 3;
                }
                if (rndOption == 3 && (!driver.findElement(By.cssSelector("mat-option:nth-of-type(" + (rndOption) + ")")).isEnabled())) {
                    rndOption = 2;
                }
                driver.findElement(By.cssSelector("mat-option:nth-of-type(" + (rndOption) + ")")).click();
                if (rndOption == 2) {
                    Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed());
                    gepekList = driver.findElements(By.cssSelector("mat-option"));
                    rndGepek = random.nextInt(gepekList.size());
                    if (gepekList.get(rndGepek).getText().equals(keresett)) {
                        s = gepekList.get(rndGepek).getText();
                    }
                    cserelt.add(dijtetelsorSzam.get(i).getText());
                    csereltEroforras.put(dijtetelsorSzam.get(i).getText(), gepekList.get(rndGepek).getText());
                    System.out.println(gepekList.get(rndGepek).getText());
                    gepekList.get(rndGepek).click();
                    driver.findElement(By.cssSelector("mat-dialog-container input[name=oraSzam]")).sendKeys(String.valueOf(random.nextInt(9) + 1));
                    driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary")).click();
                    isCsere = true;
                    csere += 1;

                }
                if (rndOption == 3) {
                    torolt.add(dijtetelsorSzam.get(i).getText());
                    isTorles = true;
                    torles += 1;
                }
            }
            System.out.println("Cserélt: " + csere + " Törölt: " + torles + " Összes: " + muveletek.size());
            Assert.assertTrue(driver.findElement(By.cssSelector("m-kapcsolodo-dijtetelek button.btn-primary")).isDisplayed());
            driver.findElement(By.cssSelector("m-kapcsolodo-dijtetelek button.btn-primary")).click();
            if (s.equals(keresett)) {
                Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed());
                System.out.println("Azonos a csere és a cserélendő erőforrás!!!");
                Assert.fail();
            }
        }
        Wait();
        List<WebElement> muveletek2 = driver.findElements(By.cssSelector("mat-cell mat-select"));
        //  Assert.assertTrue(muveletek.size() == 0);
        if (muveletek2.size() == 0) {
            Wait();
            WebElement aktivalasGomb = driver.findElement(By.cssSelector("m-subheader a.btn"));
            String before = aktivalasGomb.getText();
            aktivalasGomb.click();
            Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed());
            Assert.assertEquals(driver.findElement(By.cssSelector("mat-dialog-container h4")).getText(), "Megerősítés");

            driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary")).click();
            //    Assert.assertTrue(!driver.findElement(By.cssSelector("m-subheader a.btn")).getText().equals(before));

        }

    }

    void EllenorzesHuman() throws FileNotFoundException, InterruptedException {
        DijtetelekMain();
        Wait();
        if (isCsere) {
            System.out.println(csereltEroforras.size());

            for (Map.Entry<String, String> entry : csereltEroforras.entrySet()) {
                Wait();
                String k = entry.getKey().replace(".", "");
                String v = entry.getValue();
                System.out.println(k + " | " + v);
                driver.findElement(By.cssSelector("input[name=tetelszam]")).sendKeys(k, Keys.ENTER);
                Wait();
                List<WebElement> eredmenyek = driver.findElements(By.cssSelector("mat-cell a"));
                eredmenyek.get(0).click();
                Wait();
                Assert.assertEquals(k, driver.findElement(By.cssSelector("input[name=tetelszam]")).getAttribute("value"));
                Assert.assertEquals(driver.findElement(By.cssSelector(".m-subheader__title")).getText(), "Díjtétel szerkesztése");

                List<WebElement> eroforras = driver.findElements(By.cssSelector("input[name=humanId]"));
                for (WebElement eroforra : eroforras) {
                    if (!eroforra.getAttribute("value").equals(v)) {
                        System.out.println("A csere sikertelen");
                        Assert.fail();
                    }
                    if (eroforra.getAttribute("value").equals(keresett)) {
                        Assert.fail();
                    }
                }
                driver.navigate().back();

            }
        }
        if (isTorles) {
            Wait();
            for (String s : torolt) {
                Wait();
                String t = s.replace(".", "");
                System.out.println(t + " | " + keresett);
                driver.findElement(By.cssSelector("input[name=tetelszam]")).sendKeys(t, Keys.ENTER);
                Wait();
                List<WebElement> eredmenyek = driver.findElements(By.cssSelector("mat-cell a"));
                eredmenyek.get(0).click();
                Wait();
                Assert.assertEquals(t, driver.findElement(By.cssSelector("input[name=tetelszam]")).getAttribute("value"));
                Assert.assertEquals(driver.findElement(By.cssSelector(".m-subheader__title")).getText(), "Díjtétel szerkesztése");

                if (driver.findElement(By.cssSelector("input[name=humanId]")).isDisplayed()) {
                    List<WebElement> eroforras = driver.findElements(By.cssSelector("input[name=humanId]"));
                    for (WebElement eroforra : eroforras) {
                        if (eroforra.getAttribute("value").equals(keresett)) {
                            System.out.println("A törlés sikertelen");
                            Assert.fail();
                        }
                    }
                    System.out.println("A törlés sikeres");
                } else {
                    System.out.println("A törlés sikeres");
                }
                driver.navigate().back();
            }
        }

    }

    void EllenorzesGep() throws FileNotFoundException, InterruptedException {
        DijtetelekMain();
        Wait();
        if (isCsere) {
            System.out.println(csereltEroforras.size());

            for (Map.Entry<String, String> entry : csereltEroforras.entrySet()) {
                Wait();
                String k = entry.getKey().replace(".", "");
                String v = entry.getValue();
                System.out.println(k + " | " + v);
                driver.findElement(By.cssSelector("input[name=tetelszam]")).sendKeys(k, Keys.ENTER);
                Wait();
                List<WebElement> eredmenyek = driver.findElements(By.cssSelector("mat-cell a"));
                eredmenyek.get(0).click();
                Wait();
                Assert.assertEquals(k, driver.findElement(By.cssSelector("input[name=tetelszam]")).getAttribute("value"));
                Assert.assertEquals(driver.findElement(By.cssSelector(".m-subheader__title")).getText(), "Díjtétel szerkesztése");

                List<WebElement> eroforras = driver.findElements(By.cssSelector("input[name=gepId]"));
                for (WebElement eroforra : eroforras) {
                    if (!eroforra.getAttribute("value").equals(v)) {
                        System.out.println("A csere sikertelen");
                        Assert.fail();
                    }
                    if (eroforra.getAttribute("value").equals(keresett)) {
                        Assert.fail();
                    }
                }
                System.out.println("A csere sikeres");
                driver.navigate().back();

            }
        }
        if (isTorles) {
            Wait();
            for (String s : torolt) {
                Wait();
                String t = s.replace(".", "");
                System.out.println(t + " | " + keresett);
                driver.findElement(By.cssSelector("input[name=tetelszam]")).sendKeys(t, Keys.ENTER);
                Wait();
                List<WebElement> eredmenyek = driver.findElements(By.cssSelector("mat-cell a"));
                eredmenyek.get(0).click();
                Wait();
                Assert.assertEquals(t, driver.findElement(By.cssSelector("input[name=tetelszam]")).getAttribute("value"));
                Assert.assertEquals(driver.findElement(By.cssSelector(".m-subheader__title")).getText(), "Díjtétel szerkesztése");

                if (driver.findElement(By.cssSelector("input[name=gepId]")).isDisplayed()) {
                    List<WebElement> eroforras = driver.findElements(By.cssSelector("input[name=gepId]"));
                    for (WebElement eroforra : eroforras) {
                        if (eroforra.getAttribute("value").equals(keresett)) {
                            System.out.println("A törlés sikertelen");
                            Assert.fail();
                        }
                    }
                    System.out.println("A törlés sikeres");
                } else {
                    System.out.println("A törlés sikeres");
                }
                driver.navigate().back();
            }
        }

    }
}


