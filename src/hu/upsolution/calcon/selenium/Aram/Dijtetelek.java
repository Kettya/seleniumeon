package hu.upsolution.calcon.selenium.Aram;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Dijtetelek extends Dijtetelsor {

    @Test
    public void DijtetelsorPdfAktiv() throws FileNotFoundException, InterruptedException{
        Bejelentkezes();
        DijtetelekMain();
        DijtetelsorValasztasAktiv();
        PdfMain();
    }

    @Test
    public void DijtetelsorPdfIdozitett() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        DijtetelekMain();
        DijtetelsorValasztasIdozitett();
        PdfMain();
    }

    @Test
    public void DijtetelHozzaadasAktiv() throws InterruptedException, FileNotFoundException {
        DijtetelekMain();
        DijtetelsorValasztasAktiv();
        DijtetelHozzaadasMain();
    }

    @Test
    public void DijtetelHozzaadasIdozitett() throws FileNotFoundException, InterruptedException {
        DijtetelekMain();
        DijtetelsorValasztasIdozitett();
        DijtetelHozzaadasMain();
    }

    @Test
    public void FejezetHozzadasAktiv() throws InterruptedException, FileNotFoundException {
        Bejelentkezes();
        FejezetMain();
        DijtetelsorValasztasAktiv();
        FejezetHozzaadasMain();
    } // working

    @Test
    public void FejezetHozzadasIdozitett() throws InterruptedException, FileNotFoundException {
        Bejelentkezes();
        FejezetMain();
        DijtetelsorValasztasIdozitett();
        FejezetHozzaadasMain();
    } // working

    @Test
    public void FejezetKeresesAktiv() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        logger.info(driver.getCurrentUrl());
        FejezetMain();
        DijtetelsorValasztasAktiv();
        FejezetKeresesMain();
    } //Working

    @Test
    public void FejezetKeresesIdozitett() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        logger.info(driver.getCurrentUrl());
        FejezetMain();
        DijtetelsorValasztasIdozitett();
        FejezetKeresesMain();
    } //working

    @Test
    public void GepHozzaadasAktiv() throws InterruptedException, IOException {
        Bejelentkezes();
        GepekMain();
        DijtetelsorValasztasAktiv();
        logger.info("A jelenlegi URL: " + driver.getCurrentUrl());
        GepHozzaadasMain();
    } //working

    @Test
    public void GepHozzaadasIdozitett() throws InterruptedException, IOException {
        Bejelentkezes();
        GepekMain();
        DijtetelsorValasztasIdozitett();
        logger.info("A jelenlegi URL: " + driver.getCurrentUrl());
        GepHozzaadasMain();
    } //working

    @Test
    public void GepKeresesAktiv() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        GepekMain();
        DijtetelsorValasztasAktiv();
        GepKeresesMain();
    }

    @Test
    public void GepKeresesIdozitett() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        GepekMain();
        DijtetelsorValasztasIdozitett();
        GepKeresesMain();
    }

    @Test
    public void HumanHozzaadasAktiv() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        HumanMain();
        DijtetelsorValasztasAktiv();
        HumanHozzaadasMain();
    } //working

    @Test
    public void HumanHozzaadasIdozitett() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        HumanMain();
        DijtetelsorValasztasIdozitett();
        HumanHozzaadasMain();
    } //working

    @Test
    public void HumanKeresesAktiv() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        HumanMain();
        DijtetelsorValasztasAktiv();
        HumanKeresesMain();
    }

    @Test
    public void HumanKeresesIdozitett() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        HumanMain();
        DijtetelsorValasztasIdozitett();
        HumanKeresesMain();
    }

    @Test
    public void DijtetelKeresesAktiv() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        DijtetelekMain();
        DijtetelsorValasztasAktiv();
        DijtetelKeresesMain();
    }

    @Test
    public void DijtetelKeresesIdozitett() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        DijtetelekMain();
        DijtetelsorValasztasIdozitett();
        DijtetelKeresesMain();
    }

    @Test
    public void FejezetHozzaadasInvalidAktiv() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        FejezetMain();
        DijtetelsorValasztasAktiv();
        FejezetHozzaadasInvalidMain();
    }

    @Test
    public void FejezetHozzaadasInvalidIdozitett() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        FejezetMain();
        DijtetelsorValasztasIdozitett();
        FejezetHozzaadasInvalidMain();
    }

    @Test
    public void HumanInaktivitasAktiv() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        HumanMain();
        DijtetelsorValasztasAktiv();
        HumanKeresesMain();
        InaktivitasMain();
        EllenorzesHuman();
    }

    @Test
    public void HumanInaktivitasIdozitett() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        HumanMain();
        DijtetelsorValasztasIdozitett();
        HumanKeresesMain();
        InaktivitasMain();
        EllenorzesHuman();
    }

    @Test
    public void GepInaktivitasAktiv() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        GepekMain();
        DijtetelsorValasztasAktiv();
        GepKeresesMain();
        InaktivitasMain();
        EllenorzesGep();
    }

    @Test
    public void GepInaktivitasIdozitett() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        GepekMain();
        DijtetelsorValasztasIdozitett();
        GepKeresesMain();
        InaktivitasMain();
        EllenorzesGep();
    }
}
