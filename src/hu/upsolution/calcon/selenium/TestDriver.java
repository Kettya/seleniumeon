package hu.upsolution.calcon.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public enum TestDriver {
    CHROME(ChromeDriver.class),
    FIREFOX(FirefoxDriver.class),
    EDGE(EdgeDriver.class);

    final Class driverClass;

    TestDriver(Class driverClass) {
        this.driverClass = driverClass;
    }

    public WebDriver getWebDriver() {
        try {
            return (WebDriver) driverClass.newInstance();
        } catch (InstantiationException e) {
            return null;
        } catch (IllegalAccessException e) {
            return null;
        }
    }
}
