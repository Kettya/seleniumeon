package hu.upsolution.calcon.selenium;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.util.concurrent.TimeUnit;

public class Basic {

    protected WebDriver driver;
    protected Logger logger = LogManager.getLogger(Basic.class);
    protected WebDriverWait wait;

    private boolean bejelentkezve = false;


    @BeforeTest
    public void SetUpBasic() {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "drivers/geckodriver.exe");
        System.setProperty("webdriver.edge.driver", "drivers/MicrosoftWebDriver.exe");

        TestDriver testDriver = TestDriver.FIREFOX;

        System.setProperty("logFileName", testDriver.name().toLowerCase());

        driver = testDriver.getWebDriver();
//     driver.get("https://calcon.upsolution.hu");
        driver.get("http://10.10.1.25:91");  //main
//        driver.get("http://10.10.1.25:92");  //main másolat
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 45);
    }

    @Test
    public void Bejelentkezes() throws InterruptedException, FileNotFoundException {
        if (bejelentkezve) return;

        logger.info("Bejelentkezés tesztelése");
        //logger.info("https://calcon.upsolution.hu");
        logger.info("http://10.10.1.25:91");

        //Thread.sleep(2000);
        Assert.assertTrue(driver.findElement(By.id("username")).isDisplayed());

        ////////////////// Bejelentkezés tesztelése ///////////////////
        //áram
        String username = "aram_teszt";
        String password = "aram";

//        //gáz
//        String username = "gaz_teszt";
//        String password = "gaz";

        //System.out.println(fDriver.getCurrentUrl());
        logger.info("Jelenlegi URL: " + driver.getCurrentUrl());

        driver.findElement(By.id("username")).sendKeys(username);
        driver.findElement(By.id("password")).sendKeys(password, Keys.ENTER);

        logger.info("Bejelentkezési kisérlet");
        logger.info("Felhasználónév: " + username + ", jelszó:" + password);

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"m_header_menu\"]/ul/li[1]/a")));

        if (driver.getCurrentUrl().contains("auth")) {

            logger.error("A bejelenkezési kísérlet sikertelen");

        } else logger.info("Sikeres bejelentkezés");

       // wait.until(ExpectedConditions.urlContains("dijtetelsor"));
        Assert.assertFalse(driver.getCurrentUrl().contains("auth"));
        bejelentkezve = true;
    }
}
