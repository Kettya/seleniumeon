package hu.upsolution.calcon.selenium.Modell;

import hu.upsolution.calcon.selenium.Aram.adat.AdatKalkulacios;
import hu.upsolution.calcon.selenium.Aram.adat.AdatSzabalyok;
import hu.upsolution.calcon.selenium.Aram.adat.AdatValasztos;
import hu.upsolution.calcon.selenium.Basic;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

import static org.testng.Assert.assertEquals;


public class Kalkulacios extends Basic {
    private List<AdatKalkulacios> adatKalkulacios = new ArrayList<>();
    private List<AdatValasztos> adatValasztos = new ArrayList<>();
    private List<WebElement> jellemzokList;
    private List<AdatSzabalyok> vbszabalyok = new ArrayList<>();

    private int kivalasztottJellemzo;
    private String kivalasztottNev;

    @BeforeTest
    public void setUp() throws FileNotFoundException, InterruptedException {
        // TODO adatbeolvasás
        Scanner scanner = new Scanner(new File("adat/kalkulacios.csv"));
        while (scanner.hasNextLine()) {
            String[] split = scanner.nextLine().split(";");
            adatKalkulacios.add(new AdatKalkulacios(split[0], split[1], split[2], split[3]));
        }
        scanner.close();
        Scanner scValaszt = new Scanner(new File("adat/valasztos.csv"));
        while (scValaszt.hasNextLine()) {
            String[] split = scValaszt.nextLine().split(";");
            adatValasztos.add(new AdatValasztos(split[0], split[1], split[2], split[3], split[4], split[5], split[6], split[7], split[8], split[9], split[10]));
        }
        scValaszt.close();

        Scanner scSzabalyok = new Scanner(new File("adat/alaptipus.csv"));
        while (scSzabalyok.hasNextLine()) {
            String[] split = scSzabalyok.nextLine().split(";");
            vbszabalyok.add(new AdatSzabalyok(split[0], split[1], split[2], split[3], split[4], split[5], split[6], split[7], split[8], split[9]));
        }
        scSzabalyok.close();

        Bejelentkezes();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".m-splash-screen")));
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("a[href*=kalkulaciosmodellek]")));
        WebElement elementKalkulacio = driver.findElement(By.cssSelector("a[href*=kalkulaciosmodellek]"));
        elementKalkulacio.click();
    }

    @Test
    public void test() {
        for (AdatSzabalyok adatSzabalyok : vbszabalyok) {
            System.out.println(adatSzabalyok.getCikkszam() + " | " + adatSzabalyok.getAnyagNev() + " | " + adatSzabalyok.getSzorzok1() + " | " + adatSzabalyok.getSzorzok2() + " | " + adatSzabalyok.getSzorzok3() + " | " + adatSzabalyok.getTipus1() + " | " + adatSzabalyok.getAr());
        }
    }

    @Test
    public void kalkulacio() throws InterruptedException {


        // TODO wait

        System.out.println(driver.getCurrentUrl());


        wait.until(ExpectedConditions.urlContains("kalkulaciosmodellek"));
        Assert.assertTrue(driver.getCurrentUrl().contains("kalkulaciosmodellek"));

        driver.findElement(By.cssSelector("a[href*=uj]")).click();
        wait.until(ExpectedConditions.urlContains("uj"));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

      // Random random = new Random();

        driver.findElement(By.cssSelector("input[name='nev']")).sendKeys("KIF szig. hál. beton oszloppal");
        //endregion

        //region Folder kinyitása, kiválasztása

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("mat-tree")));

        WebElement elementMentes = driver.findElement(By.cssSelector(".btn.btn-primary[type='submit']"));
        int count = 0;
        try {
            WebElement element;
            while ((element = driver.findElement(By.cssSelector("i.tree-icon.tree-folder-closed"))) != null) {
                // ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
                element.click();
                count++;
                element.findElement(By.xpath("./..")).findElement(By.cssSelector("span")).click();
                ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,50)", "");
            }
        } catch (NoSuchElementException e) {
            Thread.sleep(3000);
        } catch (Throwable t) {
            System.err.println(t.getMessage());
        }
        System.out.println(String.format("Found'n clicked elements: %s", count));

        List<WebElement> emptyFolder = driver.findElements(By.cssSelector("i.fa-folder.empty-node"));
        for (WebElement element : emptyFolder) {
            element.click();
            Thread.sleep(1000);
            System.out.println(element.getText());
        }

//        for (int second = 0;; second++) {
//            if(second >=60){
//                break;
//            }
//            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,400)", "");
//            Thread.sleep(3000);
//        }

//        List<WebElement> allFolder = driver.findElements(By.cssSelector(".mat-tree-node span"));
//
//        int aF = random.nextInt(allFolder.size());
//        int abc = 0;
//        int rF = 0;
//
//        while (abc != aF) {
//            rF = random.nextInt(allFolder.size());
//            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", allFolder.get(rF));
//            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".m-splash-screen")));
//            allFolder.get(rF).click();
//            abc++;
//            System.out.println(allFolder.get(rF).getText());
//        }
        //endregion

        //region Modell mentése
//        WebElement elementMentes = driver.findElement(By.cssSelector(".btn.btn-primary[type='submit']"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", elementMentes);
        elementMentes.click();
        //endregion

//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".m-subheader__title")));
//        Assert.assertTrue(driver.getCurrentUrl().equals("https://calcon.upsolution.hu/kalkulaciosmodellek"));

    }

    @Test
    public void szerkesztes() {

        Random random = new Random();

        //region Kalkulációs modell kiválasztása
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

        List<WebElement> menu = driver.findElements(By.cssSelector("mat-row a.btn"));
        System.out.println(menu.size());
        menu.get(1).click();

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        driver.findElement(By.cssSelector(".mat-menu-content button:nth-of-type(1)")).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        //endregion

        //region Aktiválás
        List<WebElement> modellVerziok = driver.findElements(By.cssSelector("mat-row"));
        int rndVerzio = random.nextInt(modellVerziok.size());
        String aktiv = "Aktív";
        String piszkozat = "Piszkozat";
        String archivalt = "Archivált";
        String currentAllapot = modellVerziok.get(rndVerzio).findElement(By.cssSelector("mat-cell:nth-of-type(2)")).getText();
        System.out.println(currentAllapot);

        while (currentAllapot.equals(piszkozat) || currentAllapot.equals(aktiv)) {
            rndVerzio = random.nextInt(modellVerziok.size());
            currentAllapot = modellVerziok.get(rndVerzio).findElement(By.cssSelector("mat-cell:nth-of-type(2)")).getText();
            System.out.println(currentAllapot);
        }


        if (currentAllapot.equals(archivalt)) {
            WebElement buttonMenu = modellVerziok.get(rndVerzio).findElement(By.cssSelector("mat-cell a.btn"));
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", buttonMenu);
            buttonMenu.click();

            List<WebElement> menuList = driver.findElements(By.cssSelector(".mat-menu-content button"));
            int kivalsztottMenu = random.nextInt(menuList.size());
            System.out.println(menuList.get(kivalsztottMenu).getText());
            menuList.get(kivalsztottMenu).click();

        }
        //endregion

        //region Folder kezelés

//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("mat-tree")));
//        int count = 0;
//        try {
//            WebElement element;
//            while ((element = driver.findElement(By.cssSelector("i.tree-icon.tree-folder-closed"))) != null) {
//                element.click();
//                count++;
//                element.findElement(By.xpath("./..")).findElement(By.cssSelector("span")).click();
//            }
//        } catch (NoSuchElementException e) {
//            Thread.sleep(3000);
//        } catch (Throwable t) {
//            System.err.println(t.getMessage());
//        }
//        System.out.println(String.format("Found'n clicked elements: %s", count));
//
//        List<WebElement> emptyFolder = driver.findElements(By.cssSelector("i.fa-folder.empty-node"));
//        for (int i = 0; i < emptyFolder.size(); i++) {
//            emptyFolder.get(i).click();
//            Thread.sleep(1000);
//            i++;
//        }
//
//        List<WebElement> allFolder = driver.findElements(By.cssSelector(".mat-tree-node span"));
//
//        int aF = random.nextInt(allFolder.size());
//        int abc = 0;
//        int rF = 0;
//
//        while (abc != aF) {
//            rF = random.nextInt(allFolder.size());
//            allFolder.get(rF).click();
//            abc++;
//            System.out.println(allFolder.get(rF).getText());
//        }
        //endregion

        //region Szerkesztés és verziók
        //  String currentAllapot = driver.findElement(By.cssSelector(".mat-cell.mat-column-allapot")).getText();
        //String piszkozat = "Piszkozat";


        //endregion

    }

    @Test
    public void jellemzok() {

//        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
//        List<WebElement> menu = driver.findElements(By.cssSelector("mat-row a.btn"));
//        System.out.println(menu.size());

        int something = 1;

        while (something == 1) {
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
            //  driver.findElement(By.cssSelector(".mat-paginator-page-size-select mat-select")).click();
            //driver.findElement(By.cssSelector("mat-option:nth-of-type(5)")).click();

            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
            List<WebElement> menu = driver.findElements(By.cssSelector("mat-row a.btn"));
            System.out.println(menu.size());

            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
            menu.get(1).click();

            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
            driver.findElement(By.cssSelector(".mat-menu-content button:nth-of-type(1)")).click();
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

            Assert.assertTrue(driver.getCurrentUrl().contains("kalkulaciosmodellek/"));

            String currentAllapot = driver.findElement(By.cssSelector(".mat-cell.mat-column-allapot")).getText();
            String piszkozat = "Piszkozat";

            if (!currentAllapot.equals(piszkozat)) {
                something = 1;
                driver.navigate().back();
            } else {
                something = 2;
                System.out.println("Piszkozat");
                //TODO ".mat-menu-item:nth-of-type(1)" next
            }

        }
        List<WebElement> piszkozatList = driver.findElements(By.cssSelector(".mat-cell.mat-column-allapot"));
        for (int i = 0; i < piszkozatList.size(); i++) {
            if (piszkozatList.get(i).getText().equals("Piszkozat")) {
                driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (i + 1) + ") a.btn")).click();
            }
        }

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        driver.findElement(By.cssSelector(".mat-menu-item:nth-of-type(1)")).click();
    } //this is a main thing

    @Test
    public void jellemzokHozzaadasDirectFail() {
        jellemzok();
        Random random = new Random();

        //region Név generálás, hozzáadás
        String alphabet = "aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyz";
        //String karakterek = "§'~+^!˘%°/˛=`(˙)´<>#&@{};>*äđĐ[]łŁ$ß¤×÷Ä|˝¨¸€,.-?:_*♥☻☺♣♠♦◘";
        String letterschar = "aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyz§'~+^!˘%°/˛=`(˙)´<>#&@{};>*äđĐ[]łŁ$ß¤×÷Ä|˝¨¸€,.-?:_*♥☻☺♣♠♦◘";
        StringBuilder falseDataLetters;
        falseDataLetters = new StringBuilder();
        String justLetters;
        String charAndLetters;
        //System.out.println(karakterek.length());
        int randomLen = random.nextInt(50) + 255;
        for (int i = 0; i < randomLen; i++) {
            char c = alphabet.charAt(random.nextInt(alphabet.length()));
            falseDataLetters.append(c);
        }

        int lettersLen = random.nextInt(255);
        StringBuilder justLettersBuilder = new StringBuilder();
        for (int i = 0; i < lettersLen; i++) {
            char c = alphabet.charAt(random.nextInt(alphabet.length()));
            justLettersBuilder.append(c);
        }
        justLetters = justLettersBuilder.toString();

        int charLen = random.nextInt(255);
        StringBuilder charAndLettersBuilder = new StringBuilder();
        for (int i = 0; i < charLen; i++) {
            char c = letterschar.charAt(random.nextInt(94));
            charAndLettersBuilder.append(c);
        }
        charAndLetters = charAndLettersBuilder.toString();

        //endregion

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        Assert.assertTrue(driver.getCurrentUrl().contains("jellemzok"));

        List<WebElement> jellemzokList = driver.findElements(By.cssSelector("mat-expansion-panel-header"));
        System.out.println(jellemzokList.size());

        if ((jellemzokList.size() == 1) && jellemzokList.get(0).getText().contains("Nincs")) {
            driver.findElement(By.cssSelector("a.btn")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("mat-dialog-container")));

            driver.findElement(By.cssSelector("input[name='nev']")).sendKeys(charAndLetters);

            System.out.println("asdf");
            WebElement elementMentes = driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary"));

            if (elementMentes.isEnabled())
                elementMentes.click();
        }
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

        List<WebElement> jellemzokCsopList = driver.findElements(By.cssSelector("mat-expansion-panel-header"));
        System.out.println("bcdef");
        int rndCsoport = random.nextInt(jellemzokCsopList.size());
        jellemzokCsopList.get(rndCsoport).findElement(By.cssSelector("span.mat-expansion-indicator")).click();

        driver.findElement(By.cssSelector(".mat-expansion-panel-body a.btn")).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

        Assert.assertTrue(driver.getCurrentUrl().contains("uj"));

        driver.findElement(By.cssSelector("input[name='nev']")).sendKeys(charAndLetters);
        WebElement select = driver.findElement(By.cssSelector(".mat-select-trigger"));


        //   int rndTipus=random.nextInt(tipusSelectList.size());
        for (int i = 0; i < 5; i++) {
            select.click();

            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
            List<WebElement> tipusSelectList = driver.findElements(By.cssSelector("mat-option"));
            int selected = random.nextInt(tipusSelectList.size());
            tipusSelectList.get(selected).click();

            if (selected == 1) {
                WebElement ertekek = driver.findElement(By.cssSelector("input[name='valasztofelirat']"));
                Assert.assertTrue(ertekek.isDisplayed());
                ertekek.sendKeys(justLetters);
                WebElement ertekHozzaadas = driver.findElement(By.cssSelector("m-jellemzo-ertekek a.btn"));
                ertekHozzaadas.click();
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("mat-dialog-container")));
                driver.findElement(By.cssSelector("input[name='nev']")).sendKeys("asdfghjk");
                driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary")).click();
            }

            if (selected == 0) {
                WebElement numerik = driver.findElement(By.cssSelector("input[name='numerikfelirat']"));
                numerik.sendKeys(charAndLetters);
            }

            if (selected == 2) {
                WebElement numerik = driver.findElement(By.cssSelector("input[name='numerikfelirat']"));
                numerik.sendKeys(charAndLetters);
                WebElement ertekek = driver.findElement(By.cssSelector("input[name='valasztofelirat']"));
                Assert.assertTrue(ertekek.isDisplayed());
                ertekek.sendKeys(justLetters);
              //  WebElement ertekHozzaadas = driver.findElement(By.cssSelector("m-jellemzo-ertekek a.btn"));
                //   ertekHozzaadas.click();
                // wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("mat-dialog-container")));
                //   wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
                //  driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys("asdfghjk");
                //  driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary")).click();
            }

        }
        driver.findElement(By.cssSelector("m-jellemzo button.btn-primary")).click();

    } //working

    @Test
    public void jellemzoCsoportNevmodositas() {
        jellemzok();
        Random random = new Random();
        //TODO mat-expansion-panel i.flaticon-edit

        //region Név generálás, hozzáadás
        String alphabet = "aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyz";
      //  String karakterek = "§'~+^!˘%°/˛=`(˙)´<>#&@{};>*äđĐ[]łŁ$ß¤×÷Ä|˝¨¸€,.-?:_*♥☻☺♣♠♦◘";
        String letterschar = "aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyz§'~+^!˘%°/˛=`(˙)´<>#&@{};>*äđĐ[]łŁ$ß¤×÷Ä|˝¨¸€,.-?:_*♥☻☺♣♠♦◘";
        StringBuilder falseDataLetters;
        falseDataLetters = new StringBuilder();
        StringBuilder justLetters;
        justLetters = new StringBuilder();
        StringBuilder charAndLetters = new StringBuilder();
        //System.out.println(karakterek.length());
        int randomLen = random.nextInt(50) + 255;
        for (int i = 0; i < randomLen; i++) {
            char c = alphabet.charAt(random.nextInt(alphabet.length()));
            falseDataLetters.append(c);
        }

        int lettersLen = random.nextInt(255);
        for (int i = 0; i < lettersLen; i++) {
            char c = alphabet.charAt(random.nextInt(alphabet.length()));
            justLetters.append(c);
        }

        int charLen = random.nextInt(255);
        for (int i = 0; i < charLen; i++) {
            char c = letterschar.charAt(random.nextInt(94));
            charAndLetters.append(c);
        }

        //endregion

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        Assert.assertTrue(driver.getCurrentUrl().contains("jellemzok"));

        List<WebElement> jellemzokList = driver.findElements(By.cssSelector("mat-expansion-panel-header"));
        System.out.println(jellemzokList.size());

        if ((jellemzokList.size() == 1) && jellemzokList.get(0).getText().contains("Nincs")) {
            driver.findElement(By.cssSelector("a.btn")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("mat-dialog-container")));

            driver.findElement(By.cssSelector("input[name='nev']")).sendKeys(charAndLetters.toString());

            System.out.println("asdf");
            WebElement elementMentes = driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary"));

            if (elementMentes.isEnabled())
                elementMentes.click();
        }
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

        List<WebElement> jellemzokCsopList = driver.findElements(By.cssSelector("mat-expansion-panel-header"));
        int rndCsop = random.nextInt(jellemzokCsopList.size());
        String valtozas = "Változás";
        jellemzokCsopList.get(rndCsop).findElement(By.cssSelector("mat-expansion-panel i.flaticon-edit")).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        driver.findElement(By.cssSelector("input[name='nev']")).sendKeys(valtozas);
        WebElement elementMentes = driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary"));
        if (elementMentes.isEnabled())
            elementMentes.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        if (!jellemzokCsopList.get(rndCsop).getText().contains(valtozas)) {
            Assert.fail();
        }
    } //working, but not working

    @Test
    public void modellValasztas() {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        List<WebElement> menu = driver.findElements(By.cssSelector("mat-row a.btn"));
        System.out.println(menu.size());

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        menu.get(1).click();

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        driver.findElement(By.cssSelector(".mat-menu-content button:nth-of-type(1)")).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

        Assert.assertTrue(driver.getCurrentUrl().contains("kalkulaciosmodellek/"));

    }

    @Test
    public void ujVerzio() throws InterruptedException {
        //TODO új verzió hozzáadásaTeszt
        modellValasztas();

        List<WebElement> regiVerzio = driver.findElements(By.cssSelector("mat-row"));
        String regiVerzioNev = regiVerzio.get(0).findElement(By.cssSelector("mat-cell:nth-of-type(1)")).getText();
        String regiVerzioAllapot = regiVerzio.get(0).findElement(By.cssSelector("mat-cell:nth-of-type(2)")).getText();
        String regiVerzioDatum = regiVerzio.get(0).findElement(By.cssSelector("mat-cell:nth-of-type(3)")).getText();
        String[] split = regiVerzioNev.split("");
        int currentVerzio = Integer.parseInt(split[split.length - 1]);

        System.out.println(regiVerzioNev + " | " + regiVerzioAllapot + " | " + regiVerzioDatum);
        System.out.println(currentVerzio);

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        driver.findElement(By.cssSelector("m-subheader a.btn")).click();
        Thread.sleep(2000);
        System.out.println("Click Bait");
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        List<WebElement> ujVerzio = driver.findElements(By.cssSelector("mat-row"));
        String regiVerzioUjAllapot = ujVerzio.get(1).findElement(By.cssSelector("mat-cell:nth-of-type(2)")).getText();
        String ujVerzioNev = ujVerzio.get(0).findElement(By.cssSelector("mat-cell:nth-of-type(1)")).getText();
        String ujVerzioAllapot = ujVerzio.get(0).findElement(By.cssSelector("mat-cell:nth-of-type(2)")).getText();
        String[] split2 = ujVerzioNev.split("");
        int ujVerzioVerzio = Integer.parseInt(split2[split2.length - 1]);
        assertEquals(ujVerzioAllapot, "Piszkozat");
        assertEquals(ujVerzioVerzio - currentVerzio, 1);
        assertEquals(regiVerzioUjAllapot, "Archivált");
        System.out.println(ujVerzioVerzio - currentVerzio);


    }

    @Test
    public void jellemzoHozzaadas() {
        //TODO jellemző hozzáadása

        jellemzok();
        Random random = new Random();

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

        // Teszt failed, ha az jelenlegi url nem tartalmazza: "jellemzok"
        Assert.assertTrue(driver.getCurrentUrl().contains("jellemzok"));
        //region Csoporthozzáadása, ha nincs csoport
        List<WebElement> jellemzokList = driver.findElements(By.cssSelector("mat-expansion-panel-header"));
        if ((jellemzokList.size() == 1) && jellemzokList.get(0).getText().contains("Nincs")) {
            csoportHozzaadas();
        }
        // endregion
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        List<WebElement> jellemzokCsopList = driver.findElements(By.cssSelector("mat-expansion-panel-header"));
        int rndCsoport = random.nextInt(jellemzokCsopList.size());

        jellemzokCsopList.get(rndCsoport).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

        driver.findElement(By.cssSelector(".mat-expansion-panel-body a.btn")).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

//        if (driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed()) {
//            driver.findElement(By.cssSelector("mat-dialog-container .btn-primary")).click();
//        }
        // Teszt failed, ha az jelenlegi url nem tartalmazza: "uj"
        Assert.assertTrue(driver.getCurrentUrl().contains("uj"));
        //int rndVbokif = random.nextInt(adatKalkulacios.size());
        int rndVbokif = 0;
        int rndJelemzo = 0;

        String jellemzonev = adatKalkulacios.get(rndVbokif).getNev();
        String numerikErtek = jellemzonev + " Darabszám";
        if (!adatKalkulacios.get(rndVbokif).getValasztoDarab().equals(" ")) {
            rndJelemzo = Integer.parseInt(adatKalkulacios.get(rndVbokif).getValasztoDarab());
            System.out.println(rndJelemzo);
        }


        driver.findElement(By.cssSelector("input[name='nev']")).sendKeys(jellemzonev);
        WebElement select = driver.findElement(By.cssSelector(".mat-select-trigger"));
        select.click();

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        List<WebElement> tipusSelectList = driver.findElements(By.cssSelector("mat-option"));
        //region Numerikus és Választós
        if (!adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals(" ") && adatKalkulacios.get(rndVbokif).getNumerikus().equals("i")) {
            tipusSelectList.get(2).click();
            WebElement numerikusErtek = driver.findElement(By.cssSelector("input[name='numerikfelirat']"));
            WebElement ertekek = driver.findElement(By.cssSelector("input[name='valasztofelirat']"));
            Assert.assertTrue(ertekek.isDisplayed());
            Assert.assertTrue(numerikusErtek.isDisplayed());
            numerikusErtek.sendKeys(numerikErtek);
            ertekek.sendKeys(jellemzonev);
            for (int i = 0; i < rndJelemzo; i++) {
                WebElement ertekHozzaadas = driver.findElement(By.cssSelector("m-jellemzo-ertekek a.btn"));
                ertekHozzaadas.click();
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("mat-dialog-container")));
                if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals("A"))
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(adatValasztos.get(i).getOszlop());
                if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals("T"))
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(adatValasztos.get(i).getTalaj());
                if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals("P"))
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(adatValasztos.get(i).getAlaptipus());
                if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals("C"))
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(adatValasztos.get(i).getFoldmunka());
                if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals("K"))
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(adatValasztos.get(i).getOnhtarto());
                if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals("M"))
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(adatValasztos.get(i).getOnhfejsz());
                if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals("F"))
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(adatValasztos.get(i).getCstfelfuhhII());
                if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals("G"))
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(adatValasztos.get(i).getCstfelfuhhII());
                if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals("FF"))
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(adatValasztos.get(i).getLengotarto());
                if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals("N"))
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(adatValasztos.get(i).getVegfeszito());
                if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals("O"))
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(adatValasztos.get(i).getFejszerknull());
                driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary")).click();
            }
        }
        //endregion
        //region Választós
        if (!adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals(" ") && adatKalkulacios.get(rndVbokif).getNumerikus().equals("n")) {
            tipusSelectList.get(1).click();
            WebElement ertekek = driver.findElement(By.cssSelector("input[name='valasztofelirat']"));
            Assert.assertTrue(ertekek.isDisplayed());
            ertekek.sendKeys(jellemzonev);
            for (int i = 0; i < rndJelemzo; i++) {
                WebElement ertekHozzaadas = driver.findElement(By.cssSelector("m-jellemzo-ertekek a.btn"));
                ertekHozzaadas.click();
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("mat-dialog-container")));
                if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals("A"))
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(adatValasztos.get(i).getOszlop());
                if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals("T"))
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(adatValasztos.get(i).getTalaj());
                if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals("P"))
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(adatValasztos.get(i).getAlaptipus());
                if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals("C"))
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(adatValasztos.get(i).getFoldmunka());
                if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals("K"))
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(adatValasztos.get(i).getOnhtarto());
                if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals("M"))
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(adatValasztos.get(i).getOnhfejsz());
                if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals("F"))
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(adatValasztos.get(i).getCstfelfuhhII());
                if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals("G"))
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(adatValasztos.get(i).getCstfelfuhhII());
                if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals("FF"))
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(adatValasztos.get(i).getLengotarto());
                if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals("N"))
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(adatValasztos.get(i).getVegfeszito());
                if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals("O"))
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(adatValasztos.get(i).getFejszerknull());
                driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary")).click();
            }
        }
        //endregion
        //region Numerikus
        if (adatKalkulacios.get(rndVbokif).getValasztoFelirat().equals(" ") && adatKalkulacios.get(rndVbokif).getNumerikus().equals("i")) {
            tipusSelectList.get(0).click();
            WebElement numerikusErtek = driver.findElement(By.cssSelector("input[name='numerikfelirat']"));
            Assert.assertTrue(numerikusErtek.isDisplayed());
            numerikusErtek.sendKeys(numerikErtek);
        }
        //endregion

        driver.findElement(By.cssSelector("m-jellemzo button.btn-primary")).click();
    } //working

    @Test
    public void jellemzoCsoportTorles() {
        //TODO jellemző csoport törlése
        jellemzok();
        Random random = new Random();

        List<WebElement> csoportList = driver.findElements(By.cssSelector("mat-expansion-panel"));
        System.out.println(csoportList.size());

        if ((csoportList.size() == 1) && csoportList.get(0).getText().contains("Nincs")) {
            csoportHozzaadas();
        }

        List<WebElement> csoportListFinal = driver.findElements(By.cssSelector("mat-expansion-panel"));
        int rndCsoport = random.nextInt(csoportListFinal.size());
        String torlendoCsoport = csoportListFinal.get(rndCsoport).getText();

        csoportListFinal.get(rndCsoport).findElement(By.cssSelector(".mat-expansion-indicator")).click();
        List<WebElement> csoportJellemzok = csoportListFinal.get(rndCsoport).findElements(By.cssSelector(".mat-list-item .context-button"));

        System.out.println(csoportJellemzok.size() + " asdffggdgdgdsg " + torlendoCsoport);

        csoportListFinal.get(rndCsoport).findElement(By.cssSelector(".flaticon-delete")).click();
//        if (csoportJellemzok.size() < 1) {
//            WebElement megerosites = driver.findElement(By.cssSelector("mat-dialog-container"));
//            Assert.assertTrue(megerosites.isDisplayed());
//            Assert.assertTrue(megerosites.findElement(By.cssSelector("h4")).getText().equals("Megerősítés"));
//            megerosites.findElement(By.cssSelector("button.btn-primary")).click();
//
//            List<WebElement> afterDelete=driver.findElements(By.cssSelector("mat-expansion-panel"));
//
//            int asa=0;
//            for (int i=0; i<afterDelete.size();i++) {
//                if (afterDelete.get(i).getText().equals(torlendoCsoport)){
//                    asa+=1;
//                }
//            }
//            if (asa==0){
//                System.out.println("The delete was succesfull");
//            }
//        }
        if (csoportJellemzok.size() > 0) {
            WebElement hiba = driver.findElement(By.cssSelector("mat-dialog-container"));
            Assert.assertTrue(hiba.isDisplayed());
            Assert.assertEquals(hiba.findElement(By.cssSelector("h4")).getText(), "Hiba");
            hiba.findElement(By.cssSelector("button.btn-primary")).click();
            System.out.println("Volt jellemző");
        }

    } //részben working

    @Test
    public void jellemzoModositasa() {
        //TODO Jellemző módosítása
        jellemzok();
        Random random = new Random();
        //region Név generálás, hozzáadás
        String alphabet = "aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyz";
        String karakterek = "§'~+^!˘%°/˛=`(˙)´<>#&@{};>*äđĐ[]łŁ$ß¤×÷Ä|˝¨¸€,.-?:_*♥☻☺♣♠♦◘";
        String letterschar = "aábcdeéfghiíjklmnoóöőpqrstu úüűvwxyz§'~+^!˘%°/˛=`(˙)´<>#&@{};>*äđĐ[]łŁ$ß¤×÷Ä|˝¨¸€,.-?:_*♥☻☺♣♠♦◘";
        String justLetters;
        String charAndLetters;
        //System.out.println(karakterek.length());

        int lettersLen = random.nextInt(25);
        StringBuilder justLettersBuilder = new StringBuilder();
        for (int i = 0; i < lettersLen; i++) {
            char c = alphabet.charAt(random.nextInt(alphabet.length()));
            justLettersBuilder.append(c);
        }
        justLetters = justLettersBuilder.toString();

        int charLen = random.nextInt(25);
        StringBuilder charAndLettersBuilder = new StringBuilder();
        for (int i = 0; i < charLen; i++) {
            char c = letterschar.charAt(random.nextInt(95));
            charAndLettersBuilder.append(c);
        }
        charAndLetters = charAndLettersBuilder.toString();

        //endregion

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        List<WebElement> firstCsoportok = driver.findElements(By.cssSelector("mat-expansion-panel"));
        if (firstCsoportok.get(0).getText().contains("Nincs")) {
            csoportHozzaadas();
        }
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        List<WebElement> secondCsoportok = driver.findElements(By.cssSelector("mat-expansion-panel"));
        int rndCsoportok = random.nextInt(secondCsoportok.size());
        secondCsoportok.get(rndCsoportok).click();

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        //region Ha nincs jellemző
        List<WebElement> firstJellemzok = secondCsoportok.get(rndCsoportok).findElements(By.cssSelector("mat-list-item"));
        if (firstJellemzok.get(0).getText().contains("Üres")) {
            secondCsoportok.get(rndCsoportok).findElement(By.cssSelector(".mat-expansion-panel-body a.btn")).click();
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

            // Teszt failed, ha az jelenlegi url nem tartalmazza: "uj"
            Assert.assertTrue(driver.getCurrentUrl().contains("uj"));
            driver.findElement(By.cssSelector("input[name='nev']")).sendKeys(justLetters);
            WebElement select = driver.findElement(By.cssSelector(".mat-select-trigger"));

            select.click();
            int rndJelemzo = random.nextInt(15);

            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
            List<WebElement> tipusSelectList = driver.findElements(By.cssSelector("mat-option"));
            int selected = random.nextInt(tipusSelectList.size());
            tipusSelectList.get(selected).click();

            //region Választós
            if (selected == 1) {
                WebElement ertekek = driver.findElement(By.cssSelector("input[name='valasztofelirat']"));
                Assert.assertTrue(ertekek.isDisplayed());
                ertekek.sendKeys(justLetters);
                WebElement ertekHozzaadas = driver.findElement(By.cssSelector("m-jellemzo-ertekek a.btn"));

                for (int k = 0; k < rndJelemzo; k++) {
                    StringBuilder justLettersJellemzo = new StringBuilder();
                    for (int j = 0; j < lettersLen; j++) {
                        char c = alphabet.charAt(random.nextInt(alphabet.length()));
                        justLettersJellemzo.append(c);
                    }

                    ertekHozzaadas.click();
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("mat-dialog-container")));
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(justLettersJellemzo.toString());
                    driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary")).click();
                }
            }
            //endregion

            //region Numerikus
            if (selected == 0) {
                WebElement numerik = driver.findElement(By.cssSelector("input[name='numerikfelirat']"));
                numerik.sendKeys(charAndLetters);
            }
            //endregion

            //region Numerikus és választós
            if (selected == 2) {
                WebElement numerik = driver.findElement(By.cssSelector("input[name='numerikfelirat']"));
                numerik.sendKeys(charAndLetters);
                WebElement ertekek = driver.findElement(By.cssSelector("input[name='valasztofelirat']"));
                Assert.assertTrue(ertekek.isDisplayed());
                ertekek.sendKeys(justLetters);
                WebElement ertekHozzaadas = driver.findElement(By.cssSelector("m-jellemzo-ertekek a.btn"));
                for (int l = 0; l < rndJelemzo; l++) {
                    StringBuilder justLettersJellemzo = new StringBuilder();
                    for (int j = 0; j < lettersLen; j++) {
                        char c = alphabet.charAt(random.nextInt(alphabet.length()));
                        justLettersJellemzo.append(c);
                    }
                    ertekHozzaadas.click();
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("mat-dialog-container")));
                    wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(justLettersJellemzo.toString());
                    driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary")).click();
                }
            }
            //endregion

            driver.findElement(By.cssSelector("m-jellemzo button.btn-primary")).click();
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
            List<WebElement> abcd = driver.findElements(By.cssSelector("mat-expansion-panel"));
            abcd.get(rndCsoportok).click();
        }
        //endregion

        List<WebElement> secondJellemzok = secondCsoportok.get(rndCsoportok).findElements(By.cssSelector("mat-list-item"));
        int rndJellemzok = random.nextInt(secondJellemzok.size());
        //String rndJellemzoNev = secondJellemzok.get(rndJellemzok).getText();

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        secondJellemzok.get(rndJellemzok).findElement(By.cssSelector("a.btn")).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        driver.findElement(By.cssSelector("button.mat-menu-item:nth-of-type(1)")).click();

        //region Jellemző név módosítás
        Assert.assertTrue(driver.getCurrentUrl().contains("jellemzok/"));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

        String beforeName = driver.findElement(By.cssSelector("input[name='nev'")).getAttribute("value");
        String beforeTipus = driver.findElement(By.cssSelector("mat-select")).getText();
        String beforeNumerik;
        String beforeValaszto;
        List<WebElement> beforeValasztoList;
        boolean isTheSameNumerik = false;
        boolean isTheSameValasztos = false;
        boolean isTheSameErtek = false;

        if (beforeTipus.equals("Numerikus")) {
            beforeNumerik = driver.findElement(By.cssSelector("input[name='numerikfelirat'")).getAttribute("value");
            if (beforeName.equals(beforeNumerik)) {
                isTheSameNumerik = true;
            }
        }
        if (beforeTipus.equals("Választós")) {
            //valasztofelirat
            beforeValaszto = driver.findElement(By.cssSelector("input[name='valasztofelirat'")).getAttribute("value");
            if (beforeName.equals(beforeValaszto)) {
                isTheSameValasztos = true;
            }
            beforeValasztoList = driver.findElements(By.cssSelector(".ertek-doboz"));
            for (int i = 0; i < beforeValasztoList.size(); i++) {
                for (int j = i + 1; j < beforeValasztoList.size(); j++) {
                    if (beforeValasztoList.get(i).getText().equals(beforeValasztoList.get(j).getText())) {
                        isTheSameErtek = true;
                    }
                }
            }
            System.out.println(isTheSameErtek);

        }
        if (beforeTipus.equals("Numerikus + Választós")) {
       //     beforeNumerik = driver.findElement(By.cssSelector("input[name='numerikfelirat'")).getAttribute("value");
         //   beforeValaszto = driver.findElement(By.cssSelector("input[name='valasztofelirat'")).getAttribute("value");
            beforeValasztoList = driver.findElements(By.cssSelector(".ertek-doboz"));
            for (int i = 0; i < beforeValasztoList.size(); i++) {
                for (int j = i + 1; j < beforeValasztoList.size(); j++) {
                    if (beforeValasztoList.get(i).getText().equals(beforeValasztoList.get(j).getText())) {
                        isTheSameErtek = true;
                    }
                }
            }
            System.out.println(isTheSameErtek);
        }
        String change = "Változás" + random.nextInt(9);
        driver.findElement(By.cssSelector("input[name='nev'")).clear();
        driver.findElement(By.cssSelector("input[name='nev'")).sendKeys(change);
        driver.findElement(By.cssSelector("m-jellemzo button.btn-primary")).click();
        driver.navigate().back();

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        List<WebElement> abcd = driver.findElements(By.cssSelector("mat-expansion-panel"));
        abcd.get(rndCsoportok).click();
        //endregion

        List<WebElement> check = abcd.get(rndCsoportok).findElements(By.cssSelector("mat-list-item"));
        check.get(rndJellemzok).findElement(By.cssSelector("a.btn")).click();
        driver.findElement(By.cssSelector("button.mat-menu-item:nth-of-type(1)")).click();

        //region Jellemző ellenőrzés
        Assert.assertTrue(driver.getCurrentUrl().contains("jellemzok/"));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

        String afterName = driver.findElement(By.cssSelector("input[name='nev'")).getAttribute("value");
        String afterTipus = driver.findElement(By.cssSelector("mat-select")).getText();
        String afterNumerik;
        String afterValaszto;
        List<WebElement> afterValasztoList;
        boolean isTheSameNumerikAfter = false;
        boolean isTheSameValasztosAfter = false;
        boolean isTheSameErtekAfter = false;

        if (beforeTipus.equals("Numerikus")) {
            afterNumerik = driver.findElement(By.cssSelector("input[name='numerikfelirat'")).getAttribute("value");
            if (afterName.equals(afterNumerik)) {
                isTheSameNumerikAfter = true;
            }
        }
        if (afterTipus.equals("Választós")) {
            //valasztofelirat
            afterValaszto = driver.findElement(By.cssSelector("input[name='valasztofelirat'")).getAttribute("value");
            if (afterName.equals(afterValaszto)) {
                isTheSameValasztosAfter = true;
            }
            afterValasztoList = driver.findElements(By.cssSelector(".ertek-doboz"));
            for (int i = 0; i < afterValasztoList.size(); i++) {
                for (int j = i + 1; j < afterValasztoList.size(); j++) {
                    if (afterValasztoList.get(i).getText().equals(afterValasztoList.get(j).getText())) {
                        isTheSameErtekAfter = true;
                    }
                }
            }
            System.out.println(isTheSameErtekAfter);

        }
        if (beforeTipus.equals("Numerikus + Választós")) {
         //   afterNumerik = driver.findElement(By.cssSelector("input[name='numerikfelirat'")).getAttribute("value");
         //   afterValaszto = driver.findElement(By.cssSelector("input[name='valasztofelirat'")).getAttribute("value");
            afterValasztoList = driver.findElements(By.cssSelector(".ertek-doboz"));
            for (int i = 0; i < afterValasztoList.size(); i++) {
                for (int j = i + 1; j < afterValasztoList.size(); j++) {
                    if (afterValasztoList.get(i).getText().equals(afterValasztoList.get(j).getText())) {
                        isTheSameErtekAfter = true;
                    }
                }
            }
            System.out.println(afterValasztoList.get(0).getText());
            System.out.println(isTheSameErtekAfter);
        }
        if ((isTheSameErtekAfter && !isTheSameErtek) ||
                (isTheSameErtekAfter && isTheSameErtek) ||
                (isTheSameNumerikAfter && !isTheSameNumerik) ||
                (isTheSameValasztosAfter && !isTheSameValasztos) || (!afterName.equals(change))) {
            System.out.println("Something wrong");
            Assert.fail();
        } else {
            System.out.println("Everything is AWESOME");
        }
        //endregion
    } //this is a big big ********

    @Test
    public void csoportHozzaadas() {
//        jellemzok();
        Random random = new Random();

        //region Név generálás, hozzáadás
        String alphabet = "aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyz";
        //String karakterek = "§'~+^!˘%°/˛=`(˙)´<>#&@{};>*äđĐ[]łŁ$ß¤×÷Ä|˝¨¸€,.-?:_*♥☻☺♣♠♦◘";
        String letterschar = "aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyz§'~+^!˘%°/˛=`(˙)´<>#&@{};>*äđĐ[]łŁ$ß¤×÷Ä|˝¨¸€,.-?:_*♥☻☺♣♠♦◘";
        StringBuilder falseDataLetters;
        falseDataLetters = new StringBuilder();
        StringBuilder justLetters;
        justLetters = new StringBuilder();
        StringBuilder charAndLetters;
        charAndLetters = new StringBuilder();
        //System.out.println(karakterek.length());
        int randomLen = random.nextInt(50) + 255;
        for (int i = 0; i < randomLen; i++) {
            char c = alphabet.charAt(random.nextInt(alphabet.length()));
            falseDataLetters.append(c);
        }

        int lettersLen = random.nextInt(30);
        for (int i = 0; i < lettersLen; i++) {
            char c = alphabet.charAt(random.nextInt(alphabet.length()));
            justLetters.append(c);
        }

        int charLen = random.nextInt(255);
        for (int i = 0; i < charLen; i++) {
            char c = letterschar.charAt(random.nextInt(94));
            charAndLetters.append(c);
        }

        //endregion

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        Assert.assertTrue(driver.getCurrentUrl().contains("jellemzok"));

        driver.findElement(By.cssSelector("a.btn")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("mat-dialog-container")));

        driver.findElement(By.cssSelector("input[name='nev']")).sendKeys("Alaptípus");

        System.out.println("asdf");
        WebElement elementMentes = driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary"));

        if (elementMentes.isEnabled())
            elementMentes.click();
    } // working

    @Test
    public void csoportokDragAndDrop() {
        //TODO Drag and Drop
        jellemzok();
        Random random = new Random();

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        List<WebElement> firstCsoportList = driver.findElements(By.cssSelector("mat-expansion-panel"));
        System.out.println(firstCsoportList.size());

        if (firstCsoportList.size() == 1 && (!firstCsoportList.get(0).getText().contains("Nincs"))) {
            for (int i = 0; i < 2; i++) {
                csoportHozzaadas();
            }
        }
        if (firstCsoportList.size() == 1 && (firstCsoportList.get(0).getText().contains("Nincs"))) {
            for (int i = 0; i < 3; i++) {
                csoportHozzaadas();
            }
        }

        List<WebElement> secondCsoportList = driver.findElements(By.cssSelector("mat-expansion-panel"));
        System.out.println(secondCsoportList.size());
        int rndFrom = random.nextInt(secondCsoportList.size());
        int rndTo = random.nextInt(secondCsoportList.size());

        while (rndFrom == rndTo) {
            rndTo = random.nextInt(secondCsoportList.size());
        }

        logger.info("Drag And Drop előtt");
        for (WebElement element : secondCsoportList) {
            logger.info(element.getText());
        }

        System.out.println(rndFrom + " " + rndTo);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

        WebElement From = driver.findElement(By.cssSelector("mat-expansion-panel:nth-of-type(" + (rndFrom + 1) + ")"));
        WebElement To = driver.findElement(By.cssSelector("mat-expansion-panel:nth-of-type(" + (rndTo + 1) + ")"));

        Actions actions = new Actions(driver);
        actions.dragAndDrop(From, To).build().perform();

        boolean isSuccesfull = false;
        List<WebElement> finalCsoportList = driver.findElements(By.cssSelector("mat-expansion-panel"));
        logger.info("Drag And Drop után");
        for (int i = 0; i < finalCsoportList.size(); i++) {
            logger.info(finalCsoportList.get(i).getText());

            if (!finalCsoportList.get(i).getText().equals(secondCsoportList.get(i).getText())) {
                isSuccesfull = true;
            }
        }

        if (isSuccesfull)
            logger.info("Drag And Drop was successfull");
        else
            logger.info("Drag And Drop was UNSUCCESSFULL");


    } //working

    @Test
    public void jellemzokMozgatasa() {
        //TODO jellemzők mozgatása
        jellemzok();
        Random random = new Random();

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        List<WebElement> firstCsoportList = driver.findElements(By.cssSelector("mat-expansion-panel"));
        if (firstCsoportList.size() == 1 && firstCsoportList.get(0).getText().contains("Nincs")) {
            for (int i = 0; i < 3; i++) {
                csoportHozzaadas();
            }
        }

        if (firstCsoportList.size() == 1) {
            for (int i = 0; i < 2; i++) {
                csoportHozzaadas();
            }
        }

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        List<WebElement> csoportList = driver.findElements(By.cssSelector("mat-expansion-panel"));
        int rndCsopFrom = random.nextInt(csoportList.size());
        int rndCsopTo = random.nextInt(csoportList.size());
        while (rndCsopFrom == rndCsopTo) {
            rndCsopTo = random.nextInt(csoportList.size());
        }
        String rndCsopFromNev = csoportList.get(rndCsopFrom).getText();
        String rndCsopToNev = csoportList.get(rndCsopTo).getText();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        csoportList.get(rndCsopFrom).click();

        List<WebElement> jellemzokList = csoportList.get(rndCsopFrom).findElements(By.cssSelector("mat-list-item"));

        //region Jellemző hozzáadása ha üres a csoport
        if (jellemzokList.get(0).getText().equals("Üres csoport")) {
            //region Név generálás, hozzáadás
            String alphabet = "aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyz";
            //String karakterek = "§'~+^!˘%°/˛=`(˙)´<>#&@{};>*äđĐ[]łŁ$ß¤×÷Ä|˝¨¸€,.-?:_*♥☻☺♣♠♦◘";
            String letterschar = "aábcdeéfghiíjklmnoóöőpqrstu úüűvwxyz§'~+^!˘%°/˛=`(˙)´<>#&@{};>*äđĐ[]łŁ$ß¤×÷Ä|˝¨¸€,.-?:_*♥☻☺♣♠♦◘";
            StringBuilder falseDataLetters;
            falseDataLetters = new StringBuilder();
            String justLetters;
            String charAndLetters;
            //System.out.println(karakterek.length());
            int randomLen = random.nextInt(50) + 255;
            for (int i = 0; i < randomLen; i++) {
                char c = alphabet.charAt(random.nextInt(alphabet.length()));
                falseDataLetters.append(c);
            }

            int lettersLen = random.nextInt(25);
            StringBuilder justLettersBuilder = new StringBuilder();
            for (int i = 0; i < lettersLen; i++) {
                char c = alphabet.charAt(random.nextInt(alphabet.length()));
                justLettersBuilder.append(c);
            }
            justLetters = justLettersBuilder.toString();

            int charLen = random.nextInt(255);
            StringBuilder charAndLettersBuilder = new StringBuilder();
            for (int i = 0; i < charLen; i++) {
                char c = letterschar.charAt(random.nextInt(95));
                charAndLettersBuilder.append(c);
            }
            charAndLetters = charAndLettersBuilder.toString();

            //endregion
            driver.findElement(By.cssSelector(".mat-expansion-panel-body a.btn")).click();
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

            // Teszt failed, ha az jelenlegi url nem tartalmazza: "uj"
            Assert.assertTrue(driver.getCurrentUrl().contains("uj"));
            driver.findElement(By.cssSelector("input[name='nev']")).sendKeys(justLetters);
            WebElement select = driver.findElement(By.cssSelector(".mat-select-trigger"));

            select.click();
            int rndJelemzo = random.nextInt(3);

            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
            List<WebElement> tipusSelectList = driver.findElements(By.cssSelector("mat-option"));
            int selected = random.nextInt(tipusSelectList.size());
            tipusSelectList.get(selected).click();

            //region Választós
            if (selected == 1) {
                WebElement ertekek = driver.findElement(By.cssSelector("input[name='valasztofelirat']"));
                Assert.assertTrue(ertekek.isDisplayed());
                ertekek.sendKeys(justLetters);
                WebElement ertekHozzaadas = driver.findElement(By.cssSelector("m-jellemzo-ertekek a.btn"));

                for (int k = 0; k < rndJelemzo; k++) {
                    StringBuilder justLettersJellemzo = new StringBuilder();
                    for (int j = 0; j < lettersLen; j++) {
                        char c = alphabet.charAt(random.nextInt(alphabet.length()));
                        justLettersJellemzo.append(c);
                    }

                    ertekHozzaadas.click();
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("mat-dialog-container")));
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(justLettersJellemzo.toString());
                    driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary")).click();
                }
            }
            //endregion

            //region Numerikus
            if (selected == 0) {
                WebElement numerik = driver.findElement(By.cssSelector("input[name='numerikfelirat']"));
                numerik.sendKeys(charAndLetters);
            }
            //endregion

            //region Numerikus és választós
            if (selected == 2) {
                WebElement numerik = driver.findElement(By.cssSelector("input[name='numerikfelirat']"));
                numerik.sendKeys(charAndLetters);
                WebElement ertekek = driver.findElement(By.cssSelector("input[name='valasztofelirat']"));
                Assert.assertTrue(ertekek.isDisplayed());
                ertekek.sendKeys(justLetters);
                WebElement ertekHozzaadas = driver.findElement(By.cssSelector("m-jellemzo-ertekek a.btn"));
                for (int l = 0; l < rndJelemzo; l++) {
                    StringBuilder justLettersJellemzo = new StringBuilder();
                    for (int j = 0; j < lettersLen; j++) {
                        char c = alphabet.charAt(random.nextInt(alphabet.length()));
                        justLettersJellemzo.append(c);
                    }
                    ertekHozzaadas.click();
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("mat-dialog-container")));
                    wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
                    driver.findElement(By.cssSelector("mat-dialog-container mat-form-field input[name='nev']")).sendKeys(justLettersJellemzo.toString());
                    driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary")).click();
                }
            }
            //endregion

            driver.findElement(By.cssSelector("m-jellemzo button.btn-primary")).click();

        }
        //endregion

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        csoportList.get(rndCsopFrom).click();
        List<WebElement> jellemzokSecondList = csoportList.get(rndCsopFrom).findElements(By.cssSelector("mat-list-item"));
        int rndJellemzo = random.nextInt(jellemzokSecondList.size());
        String rndJellemzoNev = jellemzokSecondList.get(rndJellemzo).getText();
        for (WebElement element : jellemzokSecondList) {
            System.out.println(element.getText());
        }
        System.out.println(rndCsopFromNev + " " + rndJellemzoNev);
        System.out.println(rndCsopToNev);

        jellemzokSecondList.get(rndJellemzo).findElement(By.cssSelector("a.btn")).click();
        driver.findElement(By.cssSelector("button.mat-menu-item:nth-of-type(3)")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("mat-dialog-container")));

        driver.findElement(By.cssSelector("mat-dialog-container mat-select")).click();
        List<WebElement> selectList = driver.findElements(By.cssSelector("mat-option"));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

        // boolean isContain = false;
        for (WebElement element : selectList) {
            Assert.assertNotEquals(rndCsopFromNev, element.getText());
            if (element.getText().equals(rndCsopToNev)) {
                System.out.println(element.getText());
                wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
                element.click();
                //isContain = true;
            }
//            if (selectList.get(i).getText().equals(rndCsopFrom)) {
//                Assert.fail();
//            }
            // System.out.println(selectList.get(i).getText());
        }
//        if (isContain = false) {
//            Assert.fail();
//        }
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        driver.findElement(By.cssSelector(".hiba-dialog__gombok button.btn-primary")).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        csoportList.get(rndCsopTo).click();

        List<WebElement> afterMoveJellemzo = driver.findElements(By.cssSelector("mat-list-item"));
        for (WebElement element : afterMoveJellemzo) {
            if (element.getText().equals(rndJellemzoNev)) {
                System.out.println("A mozgatás sikeresen megtörtént");
            }
        }
    } //working

    @Test
    public void jellemzokDragAndDrop() {
        //TODO jellemzők szerkesztése drag and drop választós/numerikus+választós
        jellemzok();
        Random random = new Random();

        driver.findElement(By.cssSelector("mat-expansion-panel")).click();
        int a = 0;

        while (a < 30) {
            for (int i = 0; i < 10; i++) {
                List<WebElement> jellemzok = driver.findElements(By.cssSelector("mat-list-item"));
                //System.out.println("Aktiális sorrend: ");
//                for (int j = 0; j < jellemzok.size(); j++) {
//                    System.out.println(jellemzok.get(i).getText());
//                }
                int jellemzokFrom = random.nextInt(jellemzok.size());
                int jellemzokTo = random.nextInt(jellemzok.size());
                while (jellemzokFrom == jellemzokTo) {
                    jellemzokTo = random.nextInt(jellemzok.size());
                }
                WebElement From = driver.findElement(By.cssSelector("mat-list-item:nth-of-type(" + (jellemzokFrom + 1) + ")"));
                WebElement To = driver.findElement(By.cssSelector("mat-list-item:nth-of-type(" + (jellemzokTo + 1) + ")"));

                Actions actions = new Actions(driver);
                actions.dragAndDrop(From, To).build().perform();
                a++;

            }
        }

    } //working

    @Test
    public void jellemzokSzerkeszteseDragAndDrop() {
        jellemzok();
        Random random = new Random();

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        driver.findElement(By.cssSelector("mat-expansion-panel")).click();
        List<WebElement> jellemzokelementList = driver.findElements(By.cssSelector("mat-list-item"));
        //  List<WebElement> valasztos = null;
        System.out.println(jellemzokelementList.size());
        //int rndJellemzok = random.nextInt(jellemzokelementList.size());

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

        jellemzokelementList.get(2).findElement(By.cssSelector("a.btn")).click();
        driver.findElement(By.cssSelector(".mat-menu-item:nth-of-type(1)")).click();

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        List<WebElement> beforeList = driver.findElements(By.cssSelector(".ertek-doboz"));
        boolean isTheSamebefore = false;
        System.out.println(beforeList.size());
        for (int i = 0; i < beforeList.size(); i++) {
            System.out.println(beforeList.get(i).getText());
            for (int j = i + 1; j < beforeList.size(); j++) {
                if (beforeList.get(i).getText().equals(beforeList.get(j).getText())) {
                    isTheSamebefore = true;
                }
            }
        }

        int ertekekFrom = random.nextInt(beforeList.size());
        int ertekekTo = random.nextInt(beforeList.size());
        while (ertekekFrom == ertekekTo) {
            ertekekTo = random.nextInt(beforeList.size());
        }

        WebElement From = driver.findElement(By.cssSelector(".ertek-doboz:nth-of-type(" + (ertekekFrom + 1) + ")"));
        WebElement To = driver.findElement(By.cssSelector(".ertek-doboz:nth-of-type(" + (ertekekTo + 1) + ")"));

        Actions actions = new Actions(driver);
        actions.dragAndDrop(From, To).build().perform();

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        driver.findElement(By.cssSelector("m-jellemzo button.btn-primary")).click();
        driver.navigate().back();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));

        driver.findElement(By.cssSelector("mat-expansion-panel")).click();
        List<WebElement> secondList = driver.findElements(By.cssSelector("mat-list-item"));
        secondList.get(2).findElement(By.cssSelector("a.btn")).click();
        driver.findElement(By.cssSelector(".mat-menu-item:nth-of-type(1)")).click();

        boolean isTheSameAfter = false;
        List<WebElement> afterList = driver.findElements(By.cssSelector(".ertek-doboz"));
        for (int i = 0; i < afterList.size(); i++) {
            System.out.println(afterList.get(i).getText());
            for (int j = i + 1; j < afterList.size(); j++) {
                if (afterList.get(i).getText().equals(afterList.get(j).getText())) {
                    isTheSameAfter = true;
                }
            }
        }
        if (isTheSameAfter && !isTheSamebefore) {
            System.out.println("Something wrong, all is the same");
            Assert.fail();
        }
        if (isTheSameAfter) {
            System.out.println("It was the same before saving");
        }

    } //not exactly what i thought, but it's working

    @Test
    public void jellemzoKivalasztasa() {
        jellemzok();
        Random random = new Random();

        List<WebElement> csoportok = driver.findElements(By.cssSelector("mat-expansion-panel"));
        csoportok.get(0).click();
        jellemzokList = driver.findElements(By.cssSelector("mat-list-item"));
//        System.out.println(jellemzokList.size());
//        int rndJellemzo = random.nextInt(jellemzokList.size());
//        kivalasztottJellemzo = rndJellemzo;
//        kivalasztottNev = jellemzokList.get(rndJellemzo).getText();
//        jellemzokList.get(rndJellemzo).findElement(By.cssSelector("a.btn")).click();
//        System.out.println("ASA: " + jellemzokList.get(kivalasztottJellemzo).getText());

        boolean isVbOszlop = false;
        while (!isVbOszlop) {
            int rndJellemzo = random.nextInt(jellemzokList.size());
            System.out.println(jellemzokList.get(rndJellemzo).getText());
            if (jellemzokList.get(rndJellemzo).getText().equals(vbszabalyok.get(0).getSzorzok1())) {
                isVbOszlop = true;
                kivalasztottJellemzo = rndJellemzo;
                kivalasztottNev = jellemzokList.get(rndJellemzo).getText();
                jellemzokList.get(rndJellemzo).findElement(By.cssSelector("a.btn")).click();
            }
        }

    } //working, main thing

    @Test
    public void jellemzoTorlese() {
        jellemzoKivalasztasa();
        System.out.println("Törlendő jellemző: " + jellemzokList.get(kivalasztottJellemzo).getText());
        driver.findElement(By.cssSelector(".mat-menu-item:nth-of-type(4)")).click();
        Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed());
        driver.findElement(By.cssSelector("mat-dialog-container .btn-primary")).click();

        List<WebElement> csoportok = driver.findElements(By.cssSelector("mat-expansion-panel"));
        csoportok.get(0).click();
        List<WebElement> jellemzokListAfter = driver.findElements(By.cssSelector("mat-list-item"));
        System.out.println(jellemzokListAfter.size());

    } //working

    @Test
    public void aktivalasiSzabalyokMain() {
        jellemzoKivalasztasa();
        driver.findElement(By.cssSelector(".mat-menu-item:nth-of-type(2)")).click();

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        Assert.assertTrue(driver.getCurrentUrl().contains("/szabalyok"));

    }  //this is szabályok main, working

    @Test
    public void szabalyHozzaadas() {
        aktivalasiSzabalyokMain();
        Random random = new Random();
        List<WebElement> eddigiSzabalyok = driver.findElements(By.cssSelector("mat-row"));
        driver.findElement(By.cssSelector(".m-subheader a.btn")).click();

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
        Assert.assertTrue(driver.getCurrentUrl().contains("/uj"));

        driver.findElement(By.cssSelector("input[name='nev']")).sendKeys(kivalasztottNev + " szabályok " + eddigiSzabalyok.size() + 1);


        int aktualSzabaly = 2;
        String[] tipus1 = new String[0];
        String[] tipus2 = new String[0];
        String[] tipus3 = new String[0];

        if (!vbszabalyok.get(aktualSzabaly).getTipus1().equals(" ")) {
            tipus1 = vbszabalyok.get(aktualSzabaly).getTipus1().split(" ");
        }
        if (!vbszabalyok.get(aktualSzabaly).getTipus2().equals("n")) {
            tipus2 = vbszabalyok.get(aktualSzabaly).getTipus2().split(" ");
        }
        if (!vbszabalyok.get(aktualSzabaly).getTipus3().equals("n")) {
            tipus3 = vbszabalyok.get(aktualSzabaly).getTipus3().split(" ");
        }

        //region Ha egy típus.. Vagy
        if (tipus1.length >= 1 && tipus2.length == 0 && tipus3.length == 0) {
            driver.findElement(By.cssSelector("mat-select")).click();
            driver.findElement(By.cssSelector("mat-option:nth-of-type(1)")).click();

            for (int j = 0; j < tipus1.length; j++) {
                driver.findElement(By.cssSelector("query-builder a.btn:nth-of-type(1)")).click();
                driver.findElement(By.cssSelector("li:nth-of-type(" + (j + 1) + ") mat-form-field:nth-of-type(1) mat-select")).click();
                List<WebElement> feltetelOption = driver.findElements(By.cssSelector("mat-option"));
                for (WebElement element1 : feltetelOption) {
                    if (element1.getText().equals(vbszabalyok.get(aktualSzabaly).getSzorzok1())) {
                        element1.click();
                        break;
                    }
                }

                driver.findElement(By.cssSelector("li:nth-of-type(" + (j + 1) + ") mat-form-field:nth-of-type(2) mat-select")).click();
                List<WebElement> operationList = driver.findElements(By.cssSelector("mat-option"));

                for (WebElement element : operationList) {
                    if (element.getText().equals("=")) {
                        element.click();
                        break;
                    }

                }

                driver.findElement(By.cssSelector("li:nth-of-type(" + (j + 1) + ") mat-form-field:nth-of-type(3) mat-select")).click();
                List<WebElement> valasztoList = driver.findElements(By.cssSelector("mat-option"));
             //   int asa = Integer.parseInt(tipus1[j]);
                System.out.println(Integer.parseInt(tipus1[j]));
                System.out.println(valasztoList.get(Integer.parseInt(tipus1[j])).getText());
                valasztoList.get(Integer.parseInt(tipus1[j])).click();
            }
        }
        //endregion

        //region Ha több Típus
        if (tipus1.length >= 1 && tipus2.length >= 1 && tipus3.length >= 1) {
            if (tipus1.length > 1) {
                driver.findElement(By.cssSelector("query-builder a.btn:nth-of-type(2)")).click();
                driver.findElement(By.cssSelector("li:nth-of-type(1) query-builder mat-select")).click();
                driver.findElement(By.cssSelector("mat-option:nth-of-type(1)")).click();
                for (int j = 0; j < tipus1.length; j++) {
                    driver.findElement(By.cssSelector("li query-builder a.btn:nth-of-type(1)")).click();
                    driver.findElement(By.cssSelector(".q-tree-container li.q-rule:nth-of-type(" + (j + 1) + ") mat-form-field:nth-of-type(1) mat-select")).click();
                    List<WebElement> feltetelOption = driver.findElements(By.cssSelector("mat-option"));
                    for (WebElement element : feltetelOption) {
                        if (element.getText().toLowerCase().equals("vb oszlop")) {
                            element.click();
                            break;
                        }
                    }

                    driver.findElement(By.cssSelector("li:nth-of-type(" + (j + 1) + ") mat-form-field:nth-of-type(2) mat-select")).click();
                    List<WebElement> operationList = driver.findElements(By.cssSelector("mat-option"));

                    for (WebElement element : operationList) {
                        if (element.getText().equals("=")) {
                            element.click();
                            break;
                        }

                    }

                    driver.findElement(By.cssSelector("li:nth-of-type(" + (j + 1) + ") mat-form-field:nth-of-type(3) mat-select")).click();
                    List<WebElement> valasztoList = driver.findElements(By.cssSelector("mat-option"));
                  //  int asa = Integer.parseInt(tipus1[j]);
                    System.out.println(Integer.parseInt(tipus1[j]));
                    System.out.println(valasztoList.get(Integer.parseInt(tipus1[j])).getText());
                    valasztoList.get(Integer.parseInt(tipus1[j])).click();
                }


            }
            if (tipus1.length == 1) {
                driver.findElement(By.cssSelector("query-builder a.btn:nth-of-type(1)")).click();
                driver.findElement(By.cssSelector("query-builder li:nth-of-type(1)  mat-form-field:nth-of-type(1) mat-select")).click();

                List<WebElement> feltetelOption = driver.findElements(By.cssSelector("mat-option"));
                for (WebElement element : feltetelOption) {
                    if (element.getText().toLowerCase().equals("vb oszlop")) {
                        element.click();
                        break;
                    }
                }

                driver.findElement(By.cssSelector("query-builder li:nth-of-type(1)   mat-form-field:nth-of-type(2) mat-select")).click();
                List<WebElement> operationList = driver.findElements(By.cssSelector("mat-option"));

                for (WebElement element : operationList) {
                    if (element.getText().equals("=")) {
                        element.click();
                        break;
                    }

                }

                driver.findElement(By.cssSelector("query-builder li:nth-of-type(1)  mat-form-field:nth-of-type(3) mat-select")).click();
                List<WebElement> valasztoList = driver.findElements(By.cssSelector("mat-option"));
              //  int asa = Integer.parseInt(tipus1[0]);
                System.out.println(Integer.parseInt(tipus1[0]));
                System.out.println(valasztoList.get(Integer.parseInt(tipus1[0])).getText());
                valasztoList.get(Integer.parseInt(tipus1[0])).click();
            }
            if (tipus2.length > 1) {
                driver.findElement(By.cssSelector("query-builder a.btn:nth-of-type(2)")).click();
                driver.findElement(By.cssSelector("query-builder li:nth-of-type(2)  query-builder mat-select")).click();
                driver.findElement(By.cssSelector("mat-option:nth-of-type(1)")).click();
                for (int j = 0; j < tipus2.length; j++) {
                    driver.findElement(By.cssSelector("query-builder li:nth-of-type(2) query-builder a.btn:nth-of-type(1)")).click();
                    driver.findElement(By.cssSelector("query-builder li:nth-of-type(2) .q-tree-container li.q-rule:nth-of-type(" + (j + 1) + ") mat-form-field:nth-of-type(1) mat-select")).click();
                    List<WebElement> feltetelOption = driver.findElements(By.cssSelector("mat-option"));
                    for (WebElement element : feltetelOption) {
                        if (element.getText().toLowerCase().equals("talajtípus")) {
                            element.click();
                            break;
                        }
                    }

                    driver.findElement(By.cssSelector("query-builder li:nth-of-type(2) .q-tree-container li.q-rule:nth-of-type(" + (j + 1) + ") mat-form-field:nth-of-type(2) mat-select")).click();
                    List<WebElement> operationList = driver.findElements(By.cssSelector("mat-option"));

                    for (WebElement element : operationList) {
                        if (element.getText().equals("=")) {
                            element.click();
                            break;
                        }

                    }

                    driver.findElement(By.cssSelector("query-builder li:nth-of-type(2) .q-tree-container li.q-rule:nth-of-type(" + (j + 1) + ") mat-form-field:nth-of-type(3) mat-select")).click();
                    List<WebElement> valasztoList = driver.findElements(By.cssSelector("mat-option"));
                   // int asa = Integer.parseInt(tipus2[j]);
                    System.out.println(Integer.parseInt(tipus2[j]));
                    System.out.println(valasztoList.get(Integer.parseInt(tipus2[j])).getText());
                    valasztoList.get(Integer.parseInt(tipus2[j])).click();
                }

            }
            if (tipus2.length == 1) {
                driver.findElement(By.cssSelector("query-builder a.btn:nth-of-type(1)")).click();
                driver.findElement(By.cssSelector("query-builder li:nth-of-type(2)  mat-form-field:nth-of-type(1) mat-select")).click();

                List<WebElement> feltetelOption = driver.findElements(By.cssSelector("mat-option"));
                for (WebElement element : feltetelOption) {
                    if (element.getText().toLowerCase().equals("talajtípus")) {
                        element.click();
                        break;
                    }
                }

                driver.findElement(By.cssSelector("query-builder li:nth-of-type(2)   mat-form-field:nth-of-type(2) mat-select")).click();
                List<WebElement> operationList = driver.findElements(By.cssSelector("mat-option"));

                for (WebElement element : operationList) {
                    if (element.getText().equals("=")) {
                        element.click();
                        break;
                    }

                }

                driver.findElement(By.cssSelector("query-builder li:nth-of-type(2)  mat-form-field:nth-of-type(3) mat-select")).click();
                List<WebElement> valasztoList = driver.findElements(By.cssSelector("mat-option"));
                //int asa = Integer.parseInt(tipus2[0]);
                System.out.println(Integer.parseInt(tipus2[0]));
                System.out.println(valasztoList.get(Integer.parseInt(tipus2[0])).getText());
                valasztoList.get(Integer.parseInt(tipus2[0])).click();
            }
            if (tipus3.length == 1) {
                driver.findElement(By.cssSelector("query-builder a.btn:nth-of-type(1)")).click();
                driver.findElement(By.cssSelector("query-builder li:nth-of-type(3)  mat-form-field:nth-of-type(1) mat-select")).click();

                List<WebElement> feltetelOption = driver.findElements(By.cssSelector("mat-option"));
                for (WebElement element : feltetelOption) {
                    if (element.getText().toLowerCase().equals("alaptípus")) {
                        element.click();
                        break;
                    }
                }

                driver.findElement(By.cssSelector("query-builder li:nth-of-type(3)   mat-form-field:nth-of-type(2) mat-select")).click();
                List<WebElement> operationList = driver.findElements(By.cssSelector("mat-option"));

                for (WebElement element : operationList) {
                    if (element.getText().equals("=")) {
                        element.click();
                        break;
                    }

                }

                driver.findElement(By.cssSelector("query-builder li:nth-of-type(3)  mat-form-field:nth-of-type(3) mat-select")).click();
                List<WebElement> valasztoList = driver.findElements(By.cssSelector("mat-option"));
                //int asa = Integer.parseInt(tipus3[0]);
                System.out.println(Integer.parseInt(tipus3[0]));
                System.out.println(valasztoList.get(Integer.parseInt(tipus3[0])).getText());
                valasztoList.get(Integer.parseInt(tipus3[0])).click();
            }
            if (tipus3.length > 1) {
                driver.findElement(By.cssSelector("query-builder a.btn:nth-of-type(2)")).click();
                driver.findElement(By.cssSelector("query-builder li:nth-of-type(3)  query-builder mat-select")).click();
                driver.findElement(By.cssSelector("mat-option:nth-of-type(1)")).click();

                for (int j = 0; j < tipus2.length; j++) {
                    driver.findElement(By.cssSelector("query-builder li:nth-of-type(3) query-builder a.btn:nth-of-type(1)")).click();
                    driver.findElement(By.cssSelector("query-builder li:nth-of-type(3) .q-tree-container li.q-rule:nth-of-type(" + (j + 1) + ") mat-form-field:nth-of-type(1) mat-select")).click();
                    List<WebElement> feltetelOption = driver.findElements(By.cssSelector("mat-option"));
                    for (WebElement element : feltetelOption) {
                        if (element.getText().toLowerCase().equals("alaptípus")) {
                            element.click();
                            break;
                        }
                    }

                    driver.findElement(By.cssSelector("query-builder li:nth-of-type(3) .q-tree-container li.q-rule:nth-of-type(" + (j + 1) + ") mat-form-field:nth-of-type(2) mat-select")).click();
                    List<WebElement> operationList = driver.findElements(By.cssSelector("mat-option"));

                    for (WebElement element : operationList) {
                        if (element.getText().equals("=")) {
                            element.click();
                            break;
                        }

                    }

                    driver.findElement(By.cssSelector("query-builder li:nth-of-type(3) .q-tree-container li.q-rule:nth-of-type(" + (j + 1) + ") mat-form-field:nth-of-type(3) mat-select")).click();
                    List<WebElement> valasztoList = driver.findElements(By.cssSelector("mat-option"));
                   // int asa = Integer.parseInt(tipus3[j]);
                    System.out.println(Integer.parseInt(tipus3[j]));
                    System.out.println(valasztoList.get(Integer.parseInt(tipus3[j])).getText());
                    valasztoList.get(Integer.parseInt(tipus3[j])).click();
                }
            }

        }
        //endregion

        int asasdasd = 0;

        //region Ha több következmény
        for (int k = 0; k < vbszabalyok.size(); k++) {
            if (vbszabalyok.get(k).getTipus1().equals(vbszabalyok.get(aktualSzabaly).getTipus1())
                    && vbszabalyok.get(k).getTipus2().equals(vbszabalyok.get(aktualSzabaly).getTipus2())
                    && vbszabalyok.get(k).getTipus3().equals(vbszabalyok.get(aktualSzabaly).getTipus3()) && (k != aktualSzabaly) && (!vbszabalyok.get(aktualSzabaly).getKovetkezmeny().equals(vbszabalyok.get(k).getKovetkezmeny()))) {
                asasdasd = 1;

//                vbszabalyok.get(aktualSzabaly).getKovetkezmeny();
                driver.findElement(By.cssSelector(".eon-kovetelmenyek-gombok a.btn:nth-of-type(1)")).click();
                wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
                //   Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed());

                WebElement anyagok = driver.findElement(By.cssSelector("input[name=anyag]"));
                anyagok.click();
                //  anyagok.sendKeys(vbszabalyok.get(aktualSzabaly).getAnyagNev().trim());
                wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
                List<WebElement> anyagList = driver.findElements(By.cssSelector("mat-option"));

//////////////////////////////////////////////
                anyagList.get(random.nextInt(anyagList.size())).click();


                WebElement szorzok = driver.findElement(By.cssSelector("mat-dialog-container mat-select"));
                szorzok.click();
                List<WebElement> szorzoklist = driver.findElements(By.cssSelector("mat-option"));
                for (WebElement element : szorzoklist) {
                    if (element.getText().contains(vbszabalyok.get(k).getSzorzok1()) && element.getText().contains("Darabszám")) {
                        element.click();
                        break;
                    }
                }
                WebElement bevitel = driver.findElement(By.cssSelector("input[name=bevitel]"));
                if (!vbszabalyok.get(aktualSzabaly).getSzorzok2().equals("")) {
                    bevitel.sendKeys("*", Keys.ENTER);
                    szorzok.click();
                    List<WebElement> szorzoklist2 = driver.findElements(By.cssSelector("mat-option"));
                    for (int i = 0; i < szorzoklist.size(); i++) {
                        if (szorzoklist2.get(i).getText().contains(vbszabalyok.get(k).getSzorzok2())) {
                            szorzoklist2.get(i).click();
                            break;
                        }
                    }
                    //  bevitel.sendKeys(vbszabalyok.get(aktualSzabaly).getSzorzok2(), Keys.ENTER);
                }
                if (!vbszabalyok.get(aktualSzabaly).getSzorzok3().equals("")) {
                    bevitel.sendKeys("*", Keys.ENTER);
                    bevitel.sendKeys(vbszabalyok.get(aktualSzabaly).getSzorzok3(), Keys.ENTER);
                }
//        bevitel.sendKeys("*", Keys.ENTER);
//        bevitel.sendKeys(vbszabalyok.get(aktualSzabaly).getAr(), Keys.ENTER);
                WebElement hozzaadas = driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary"));
                if (hozzaadas.isEnabled()) {
                    hozzaadas.click();
                }


                driver.findElement(By.cssSelector(".eon-kovetelmenyek-gombok a.btn:nth-of-type(2)")).click();
                Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed());

                WebElement szolgaltatas = driver.findElement(By.cssSelector("input[name=dijtetel]"));
                szolgaltatas.click();
                //szolgaltatas.sendKeys(vbszabalyok.get(k).getAnyagNev().trim().toLowerCase().split(",")[0]);
                wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
                List<WebElement> szolgList = driver.findElements(By.cssSelector("mat-option"));


                szolgList.get(random.nextInt(szolgList.size())).click();

                WebElement szorzokS = driver.findElement(By.cssSelector("mat-dialog-container mat-select"));
                szorzokS.click();
                List<WebElement> szorzokListS = driver.findElements(By.cssSelector("mat-option"));
                for (WebElement szorzokList : szorzokListS) {
                    if (szorzokList.getText().contains(vbszabalyok.get(k).getSzorzok1()) && szorzokList.getText().contains("Darabszám")) {
                        szorzokList.click();
                        break;
                    }
                }
                WebElement bevitelS = driver.findElement(By.cssSelector("input[name=bevitel]"));
                if (!vbszabalyok.get(k).getSzorzok2().equals("")) {
                    bevitelS.sendKeys("*", Keys.ENTER);
                    szorzokS.click();
                    for (WebElement szorzokList : szorzokListS) {
                        if (szorzokList.getText().contains(vbszabalyok.get(k).getSzorzok2())) {
                            szorzokList.click();
                            break;
                        }
                    }
                }
                if (!vbszabalyok.get(k).getSzorzok3().equals("")) {
                    bevitelS.sendKeys("*", Keys.ENTER);
                    bevitelS.sendKeys(vbszabalyok.get(k).getSzorzok3(), Keys.ENTER);
                }
//        bevitel.sendKeys("*", Keys.ENTER);
//        bevitel.sendKeys(vbszabalyok.get(aktualSzabaly).getAr(), Keys.ENTER);
                WebElement hozzaadasS = driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary"));
                if (hozzaadasS.isEnabled()) {
                    hozzaadasS.click();
                }
                break;
            }

        }
        //endregion

        //region Csak 1 következmény
        if (asasdasd == 0) {
            System.out.println("nothing");
            String kovetkezmeny = vbszabalyok.get(aktualSzabaly).getKovetkezmeny();
            if (kovetkezmeny.equals("anyag")) {

                driver.findElement(By.cssSelector(".eon-kovetelmenyek-gombok a.btn:nth-of-type(1)")).click();
                wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
                Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed());

                WebElement anyagok = driver.findElement(By.cssSelector("input[name=anyag]"));
                anyagok.click();
                //  anyagok.sendKeys(vbszabalyok.get(aktualSzabaly).getAnyagNev().trim().toLowerCase());
                wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
                List<WebElement> anyagList = driver.findElements(By.cssSelector("mat-option"));

///////////////
                anyagList.get(random.nextInt(anyagList.size())).click();

                WebElement szorzok = driver.findElement(By.cssSelector("mat-dialog-container mat-select"));
                szorzok.click();
                List<WebElement> szorzokList = driver.findElements(By.cssSelector("mat-option"));
                for (WebElement element : szorzokList) {
                    if (element.getText().contains(vbszabalyok.get(aktualSzabaly).getSzorzok1()) && element.getText().contains("Darabszám")) {
                        element.click();
                        break;
                    }
                }
                WebElement bevitel = driver.findElement(By.cssSelector("input[name=bevitel]"));
                if (!vbszabalyok.get(aktualSzabaly).getSzorzok2().equals("")) {
                    bevitel.sendKeys("*", Keys.ENTER);
                    bevitel.sendKeys(vbszabalyok.get(aktualSzabaly).getSzorzok2(), Keys.ENTER);
                }
                if (!vbszabalyok.get(aktualSzabaly).getSzorzok3().equals("")) {
                    bevitel.sendKeys("*", Keys.ENTER);
                    bevitel.sendKeys(vbszabalyok.get(aktualSzabaly).getSzorzok3(), Keys.ENTER);
                }
////        bevitel.sendKeys("*", Keys.ENTER);
////        bevitel.sendKeys(vbszabalyok.get(aktualSzabaly).getAr(), Keys.ENTER);
                WebElement hozzaadas = driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary"));
                if (hozzaadas.isEnabled()) {
                    hozzaadas.click();
                }
            }
            if (kovetkezmeny.equals("szolgáltatás")) {
                driver.findElement(By.cssSelector(".eon-kovetelmenyek-gombok a.btn:nth-of-type(2)")).click();
                Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed());

                WebElement szolgaltatas = driver.findElement(By.cssSelector("input[name=dijtetel]"));
                szolgaltatas.click();
                ///  szolgaltatas.sendKeys(vbszabalyok.get(aktualSzabaly).getAnyagNev().trim());
                wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash-screen")));
                List<WebElement> anyagList = driver.findElements(By.cssSelector("mat-option"));

///////////////////////////////////////////////////////////////
                anyagList.get(random.nextInt(anyagList.size())).click();

                WebElement szorzok = driver.findElement(By.cssSelector("mat-dialog-container mat-select"));
                szorzok.click();
                List<WebElement> szorzokList = driver.findElements(By.cssSelector("mat-option"));
                for (WebElement element : szorzokList) {
                    if (element.getText().contains(vbszabalyok.get(aktualSzabaly).getSzorzok1()) && element.getText().contains("Darabszám")) {
                        element.click();
                        break;
                    }
                }
                WebElement bevitel = driver.findElement(By.cssSelector("input[name=bevitel]"));
                if (!vbszabalyok.get(aktualSzabaly).getSzorzok2().equals("")) {
                    szorzok.click();
                    for (WebElement element : szorzokList) {
                        if (element.getText().contains(vbszabalyok.get(aktualSzabaly).getSzorzok2())) {
                            element.click();
                            break;
                        }
                    }

                }
                if (!vbszabalyok.get(aktualSzabaly).getSzorzok3().equals("")) {
                    bevitel.sendKeys("*", Keys.ENTER);
                    bevitel.sendKeys(vbszabalyok.get(aktualSzabaly).getSzorzok3(), Keys.ENTER);
                }
////        bevitel.sendKeys("*", Keys.ENTER);
////        bevitel.sendKeys(vbszabalyok.get(aktualSzabaly).getAr(), Keys.ENTER);
                WebElement hozzaadas = driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary"));
                if (hozzaadas.isEnabled()) {
                    hozzaadas.click();
                }

            }
        }
        //endregion

//
        driver.findElement(By.cssSelector(".hiba-dialog__gombok .btn-primary")).click();

    } //it will be good, and working


}



