package hu.upsolution.calcon.selenium.Kivitelezok;

import hu.upsolution.calcon.selenium.Basic;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Kivitelezok extends Basic {
    List<String> kivitelezok = new ArrayList<>();
    List<String> rezsi = new ArrayList<>();
    int kivalasztottKivitelezo;
    int kivalasztottUzem;
    String gyarszamB;
    String keretSzB;
    String raktarB;
    String szallitoB;
    String szazalekB;
    String gyarszamA;
    String keretSzA;
    String raktarA;
    String szallitoA;
    String szazalekA;


    @BeforeTest
    public void Setup() throws FileNotFoundException {
        Scanner scKivitelezo = new Scanner(new File("adat/kivitelezok.csv"));
        while (scKivitelezo.hasNextLine()) {
            kivitelezok.add(scKivitelezo.nextLine());
        }
        scKivitelezo.close();
        Scanner scRezsi = new Scanner(new File("adat/rezsianyag.csv"));
        while (scRezsi.hasNextLine()) {
            rezsi.add(scRezsi.nextLine());
        }
        scRezsi.close();

    }

    public void KivitelezokMain() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
        driver.findElement(By.cssSelector("a[href*=kivitelezok]")).click();

    }

    public void KivitelezoSzerkesztesMain() throws FileNotFoundException, InterruptedException {
        KivitelezokMain();

        List<WebElement> kivitelezokList = driver.findElements(By.cssSelector("mat-cell a.btn"));
        kivalasztottKivitelezo = 1;
        kivitelezokList.get(kivalasztottKivitelezo).click();
    }

    public void RezsianyagMain() throws FileNotFoundException, InterruptedException {
        KivitelezoSzerkesztesMain();

        WebElement rezsianyag = driver.findElement(By.cssSelector("a[href*=rezsianyagok]"));
        rezsianyag.click();

    }

    public void UzemSzerkeszteseMain() throws FileNotFoundException, InterruptedException {
        KivitelezoSzerkesztesMain();
        Random random = new Random();
        //region Random szám generátor
        String keretszerzodes = "";
        String szallitoiszam = "";
        String gyarszam = "";
        String raktarhely = "";
        String betuk = "abcdefghijklmnopqrstuvxyz";
        String szamok = "0123456789";

        //region Keretszerződészám
        for (int i = 0; i < 3; i++) {
            char c = betuk.charAt(random.nextInt(betuk.length()));
            keretszerzodes += c;
        }
        for (int i = 0; i < 6; i++) {
            char c = szamok.charAt(random.nextInt(szamok.length()));
            keretszerzodes += c;
        }
        for (int i = 0; i < 1; i++) {
            char c = betuk.charAt(random.nextInt(betuk.length()));
            keretszerzodes += c;
        }
        //endregion
        //region Szállítói szám
        for (int i = 0; i < 2; i++) {
            char c = betuk.charAt(random.nextInt(betuk.length()));
            szallitoiszam += c;
        }
        szallitoiszam += "-";
        for (int i = 0; i < 7; i++) {
            char c = szamok.charAt(random.nextInt(szamok.length()));
            szallitoiszam += c;
        }
        szallitoiszam += "-" + betuk.charAt(random.nextInt(betuk.length()));
        //endregion+
        //region Gyár száma
        for (int i = 0; i < 4; i++) {
            char c = szamok.charAt(random.nextInt(szamok.length()));
            gyarszam += c;
        }
        gyarszam += "-" + szamok.charAt(random.nextInt(szamok.length()));
        //endregion
        //region Raktárhely száma
        for (int i = 0; i < 4; i++) {
            char c = szamok.charAt(random.nextInt(szamok.length()));
            raktarhely += c;
        }
        raktarhely += "-";
        for (int i = 0; i < 2; i++) {
            char c = betuk.charAt(random.nextInt(betuk.length()));
            raktarhely += c;
        }
        //endregion

        //endregion

        List<WebElement> uzemek = driver.findElements(By.cssSelector("mat-cell a.btn"));
        kivalasztottUzem = random.nextInt(uzemek.size());
        gyarszamB = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (kivalasztottUzem + 1) + ") mat-cell:nth-of-type(2)")).getText();
        keretSzB = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (kivalasztottUzem + 1) + ") mat-cell:nth-of-type(3)")).getText();
        raktarB = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (kivalasztottUzem + 1) + ") mat-cell:nth-of-type(4)")).getText();
        szallitoB = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (kivalasztottUzem + 1) + ") mat-cell:nth-of-type(5)")).getText();
        szazalekB = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (kivalasztottUzem + 1) + ") mat-cell:nth-of-type(6)")).getText();
        System.out.println(gyarszamB + " | " + keretSzB + " | " + raktarB + " | " + szallitoB + " | " + szazalekB);
        uzemek.get(kivalasztottUzem).click();

        Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed());
        Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container h1")).getText().equals("Üzem szerkesztése"));

        WebElement uzemnev = driver.findElement(By.cssSelector("input[name=megbizo]"));
        WebElement keretSz = driver.findElement(By.cssSelector("input[name=keretszerzodesSzam]"));
        WebElement szallitoSz = driver.findElement(By.cssSelector("input[name=szallitoiSzam]"));
        WebElement gyarSz = driver.findElement(By.cssSelector("input[name=gyarszam]"));
        WebElement raktar = driver.findElement(By.cssSelector("input[name=raktarhelySzam]"));
        WebElement szazalek = driver.findElement(By.cssSelector("input[name=vallalasiszazalek]"));
        WebElement hozzaad = driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary"));
        WebElement mentes = driver.findElement(By.cssSelector("button.btn-primary"));

        keretSz.clear();
        szallitoSz.clear();
        gyarSz.clear();
        raktar.clear();
        szazalek.clear();

        keretSz.sendKeys(keretszerzodes.toUpperCase());
        szallitoSz.sendKeys(szallitoiszam.toUpperCase());
        gyarSz.sendKeys(gyarszam.toUpperCase());
        raktar.sendKeys(raktarhely.toUpperCase());
        szazalek.sendKeys(String.valueOf(random.nextInt(200) + 1));

        hozzaad.click();

        gyarszamA = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (kivalasztottUzem + 1) + ") mat-cell:nth-of-type(2)")).getText();
        keretSzA = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (kivalasztottUzem + 1) + ") mat-cell:nth-of-type(3)")).getText();
        raktarA = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (kivalasztottUzem + 1) + ") mat-cell:nth-of-type(4)")).getText();
        szallitoA = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (kivalasztottUzem + 1) + ") mat-cell:nth-of-type(5)")).getText();
        szazalekA = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (kivalasztottUzem + 1) + ") mat-cell:nth-of-type(6)")).getText();

        if (gyarszamA.equals(gyarszamB) || keretSzA.equals(keretSzB) || raktarA.equals(raktarB) || szallitoA.equals(szallitoB)) {
            Assert.fail();
        }
        System.out.println("Everything fine");
        //TODO külső változó

    }

    @Test
    public void KivitelezoHozzaadasUzemNelkul() throws FileNotFoundException, InterruptedException {
        KivitelezokMain();
        Random random = new Random();

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
        driver.findElement(By.cssSelector("m-subheader a.btn")).click();
        Assert.assertTrue(driver.findElement(By.cssSelector("m-subheader h3")).getText().equals("Új kivitelező hozzáadása"));
        WebElement cegnev = driver.findElement(By.cssSelector("input[name=kivitelezoNev]"));
        WebElement tipus = driver.findElement(By.cssSelector("mat-select[name=kivitelezoTipus]"));
        WebElement mentes = driver.findElement(By.cssSelector("m-uj-kivitelezo button.btn-primary"));
        List<WebElement> tipusList;

        cegnev.sendKeys(kivitelezok.get(5));
        tipus.click();
        tipusList = driver.findElements(By.cssSelector("mat-option"));
        tipusList.get(2).click();
        mentes.click();

        Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed());
        logger.info("Nincs üzem megadva: hibaüzenet: OK");


    } //working

    @Test
    public void Kivitelezohozzaadas() throws FileNotFoundException, InterruptedException {
        KivitelezokMain();
        Random random = new Random();

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
        driver.findElement(By.cssSelector("m-subheader a.btn")).click();
        Assert.assertTrue(driver.findElement(By.cssSelector("m-subheader h3")).getText().equals("Új kivitelező hozzáadása"));
        WebElement cegnev = driver.findElement(By.cssSelector("input[name=kivitelezoNev]"));
        WebElement tipus = driver.findElement(By.cssSelector("mat-select[name=kivitelezoTipus]"));
        WebElement ujUzem = driver.findElement(By.cssSelector(".eon-kovetelmenyek-gombok a.btn"));
        WebElement mentes = driver.findElement(By.cssSelector("m-uj-kivitelezo button.btn-primary"));
        List<WebElement> tipusList;

        cegnev.sendKeys(kivitelezok.get(5));
        tipus.click();
        tipusList = driver.findElements(By.cssSelector("mat-option"));
        tipusList.get(2).click();
        int rndUzem = random.nextInt(6);
        ujUzem.click();
        Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed());

        //region Új üzem hozzáadása
        //TODO legördülő lista ellenőrzés
        //region Random szám generátor
        String keretszerzodes = "";
        String szallitoiszam = "";
        String gyarszam = "";
        String raktarhely = "";
        String betuk = "abcdefghijklmnopqrstuvxyz";
        String szamok = "0123456789";

        //region Keretszerződészám
        for (int i = 0; i < 3; i++) {
            char c = betuk.charAt(random.nextInt(betuk.length()));
            keretszerzodes += c;
        }
        for (int i = 0; i < 6; i++) {
            char c = szamok.charAt(random.nextInt(szamok.length()));
            keretszerzodes += c;
        }
        for (int i = 0; i < 1; i++) {
            char c = betuk.charAt(random.nextInt(betuk.length()));
            keretszerzodes += c;
        }
        //endregion
        //region Szállítói szám
        for (int i = 0; i < 2; i++) {
            char c = betuk.charAt(random.nextInt(betuk.length()));
            szallitoiszam += c;
        }
        szallitoiszam += "-";
        for (int i = 0; i < 7; i++) {
            char c = szamok.charAt(random.nextInt(szamok.length()));
            szallitoiszam += c;
        }
        szallitoiszam += "-" + betuk.charAt(random.nextInt(betuk.length()));
        //endregion+
        //region Gyár száma
        for (int i = 0; i < 4; i++) {
            char c = szamok.charAt(random.nextInt(szamok.length()));
            gyarszam += c;
        }
        gyarszam += "-" + szamok.charAt(random.nextInt(szamok.length()));
        //endregion
        //region Raktárhely száma
        for (int i = 0; i < 4; i++) {
            char c = szamok.charAt(random.nextInt(szamok.length()));
            raktarhely += c;
        }
        raktarhely += "-";
        for (int i = 0; i < 2; i++) {
            char c = betuk.charAt(random.nextInt(betuk.length()));
            raktarhely += c;
        }
        //endregion

        //endregion

        WebElement uzemnev = driver.findElement(By.cssSelector("input[name=megbizo]"));
        WebElement keretSz = driver.findElement(By.cssSelector("input[name=keretszerzodesSzam]"));
        WebElement szallitoSz = driver.findElement(By.cssSelector("input[name=szallitoiSzam]"));
        WebElement gyarSz = driver.findElement(By.cssSelector("input[name=gyarszam]"));
        WebElement raktar = driver.findElement(By.cssSelector("input[name=raktarhelySzam]"));
        WebElement szazalek = driver.findElement(By.cssSelector("input[name=vallalasiszazalek]"));
        WebElement hozzaad = driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary"));
        List<WebElement> uzemek;

        uzemnev.click();
        uzemek = driver.findElements(By.cssSelector("mat-option"));
        uzemek.get(2).click();
        keretSz.sendKeys(keretszerzodes.toUpperCase());
        szallitoSz.sendKeys(szallitoiszam.toUpperCase());
        gyarSz.sendKeys(gyarszam.toUpperCase());
        raktar.sendKeys(raktarhely.toUpperCase());
        szazalek.sendKeys(String.valueOf(random.nextInt(200) + 1));
        if (hozzaad.isEnabled())
            hozzaad.click();

        //endregion

        if (mentes.isEnabled())
            mentes.click();
    } //working

    @Test
    public void RezsianyagHozzaadas() throws FileNotFoundException, InterruptedException {
        RezsianyagMain();
        Random random = new Random();
        int a = 0;
        while (true) {

            WebElement pluszRezsianyag = driver.findElement(By.cssSelector(".eon-kovetelmenyek-gombok a.btn"));
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", pluszRezsianyag);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
            pluszRezsianyag.click();

            Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed());
            Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container h1")).getText().equals("Rezsianyag hozzáadás:"));
            WebElement rezsiNev = driver.findElement(By.cssSelector("input[name=anyagId]"));
            WebElement hozzaad = driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary"));
            List<WebElement> anyagok;
            int abc = 0;

            while (abc == 0) {
                rezsiNev.clear();
                int rndAnyag = random.nextInt(rezsi.size());
                System.out.println(rezsi.get(rndAnyag));
                System.out.println(rezsi.get(rndAnyag).split(" ")[0]);
                rezsiNev.sendKeys(rezsi.get(rndAnyag).split(" ")[0].toLowerCase());
                wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
                anyagok = driver.findElements(By.cssSelector("mat-option"));
                if (anyagok.size() > 0) {
                    abc = 1;
                    anyagok.get(random.nextInt(anyagok.size())).click();
                }
            }

            if (hozzaad.isEnabled())
                hozzaad.click();
            System.out.println(a + 1);
            a++;
        }

    } //working

    @Test
    public void RezsianyagTorles() throws FileNotFoundException, InterruptedException, AWTException {
        RezsianyagMain();

        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_F12);

        Random random = new Random();
        int a = 0;
        while (true) {
            List<WebElement> anyagnev = driver.findElements(By.cssSelector("mat-row mat-cell:nth-of-type(1)"));
            List<WebElement> torles = driver.findElements(By.cssSelector("mat-row mat-cell:nth-of-type(5) a.btn"));
            if (torles.size() == 0)
                return;
            int rndAnyag = random.nextInt(torles.size());
            //  System.out.println(rndAnyag);
            System.out.println(a + 1);
            String torlendo = anyagnev.get(0).getText();
            System.out.println(torlendo);
            torles.get(0).click();
            Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed());
            driver.findElement(By.cssSelector("mat-dialog-container .hiba-dialog__gombok button.btn-primary")).click();
            List<WebElement> afterTorles = driver.findElements(By.cssSelector("mat-row mat-cell:nth-of-type(1)"));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
//            if (afterTorles.get(rndAnyag).getText().equals(torlendo)) {
//                System.out.println("Sikertelen törlés");
//                Assert.fail();
//            }
            System.out.println("Sikeresen törölve: " + torlendo);
            a++;

        }
    } //working

    @Test
    public void NavigateOsszeVissza() throws FileNotFoundException, InterruptedException {
        RezsianyagMain();

        driver.navigate().back();
        driver.navigate().back();
        driver.navigate().back();
        driver.navigate().forward();
        driver.navigate().forward();
        driver.navigate().back();
        System.out.println(driver.getCurrentUrl());
    }

    @Test
    public void Kereses() throws FileNotFoundException, InterruptedException {
        KivitelezokMain();

        Random random = new Random();
        WebElement cegnev = driver.findElement(By.cssSelector("input[name=kivitelezoNev]"));
        WebElement tipus = driver.findElement(By.cssSelector("mat-select"));
        WebElement torles = driver.findElement(By.cssSelector("span i.material-icons"));
        WebElement kereses = driver.findElement(By.cssSelector("m-kivitelezok button.btn-primary"));

        while (true) {
            List<WebElement> results;
            int rndKeres = random.nextInt(3);
            if (rndKeres == 0) {
                torles.click();
                int rndKivitelezo = random.nextInt(kivitelezok.size());
                String kivitelezo = kivitelezok.get(rndKivitelezo).split("")[0].toLowerCase();
                cegnev.sendKeys(kivitelezo, Keys.ENTER);
                wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
                results = driver.findElements(By.cssSelector("mat-row "));
                System.out.println("Keresés: " + kivitelezo + " | " + results.size() + " db találat");
            }
            if (rndKeres == 1) {
                torles.click();
                tipus.click();
                List<WebElement> tipusok = driver.findElements(By.cssSelector("mat-option"));
                int rndTipus = random.nextInt(tipusok.size());
                String tipusText = tipusok.get(rndTipus).getText();
                tipusok.get(rndTipus).click();
                kereses.click();
                wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
                results = driver.findElements(By.cssSelector("mat-row "));
                System.out.println("Keresés: " + tipusText + " | " + results.size() + " db találat");
            }
            if (rndKeres == 2) {
                torles.click();
                int rndKivitelezo = random.nextInt(kivitelezok.size());
                String kivitelezo = kivitelezok.get(rndKivitelezo).split("")[0].toLowerCase();
                cegnev.sendKeys(kivitelezo);
                tipus.click();
                List<WebElement> tipusok = driver.findElements(By.cssSelector("mat-option"));
                int rndTipus = random.nextInt(tipusok.size());
                String tipusText = tipusok.get(rndTipus).getText();
                tipusok.get(rndTipus).click();
                kereses.click();
                wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
                results = driver.findElements(By.cssSelector("mat-row"));
                System.out.println("Keresés: " + kivitelezo + " | " + tipusText + " | " + results.size() + " db találat");
            }
        }


    }

    @Test
    public void Uzemhozzaadas() throws FileNotFoundException, InterruptedException {
        KivitelezoSzerkesztesMain();
        Random random = new Random();
        WebElement mentes = driver.findElement(By.cssSelector("m-uj-kivitelezo button.btn-primary"));
        WebElement ujUzem = driver.findElement(By.cssSelector(".eon-kovetelmenyek-gombok a.btn"));

        //region Új üzem hozzáadása
        //region Random szám generátor
        String keretszerzodes = "";
        String szallitoiszam = "";
        String gyarszam = "";
        String raktarhely = "";
        String betuk = "abcdefghijklmnopqrstuvxyz";
        String szamok = "0123456789";

        //region Keretszerződészám
        for (int i = 0; i < 3; i++) {
            char c = betuk.charAt(random.nextInt(betuk.length()));
            keretszerzodes += c;
        }
        for (int i = 0; i < 6; i++) {
            char c = szamok.charAt(random.nextInt(szamok.length()));
            keretszerzodes += c;
        }
        for (int i = 0; i < 1; i++) {
            char c = betuk.charAt(random.nextInt(betuk.length()));
            keretszerzodes += c;
        }
        //endregion
        //region Szállítói szám
        for (int i = 0; i < 2; i++) {
            char c = betuk.charAt(random.nextInt(betuk.length()));
            szallitoiszam += c;
        }
        szallitoiszam += "-";
        for (int i = 0; i < 7; i++) {
            char c = szamok.charAt(random.nextInt(szamok.length()));
            szallitoiszam += c;
        }
        szallitoiszam += "-" + betuk.charAt(random.nextInt(betuk.length()));
        //endregion+
        //region Gyár száma
        for (int i = 0; i < 4; i++) {
            char c = szamok.charAt(random.nextInt(szamok.length()));
            gyarszam += c;
        }
        gyarszam += "-" + szamok.charAt(random.nextInt(szamok.length()));
        //endregion
        //region Raktárhely száma
        for (int i = 0; i < 4; i++) {
            char c = szamok.charAt(random.nextInt(szamok.length()));
            raktarhely += c;
        }
        raktarhely += "-";
        for (int i = 0; i < 2; i++) {
            char c = betuk.charAt(random.nextInt(betuk.length()));
            raktarhely += c;
        }
        //endregion

        //endregion
        ujUzem.click();
        Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed());

        WebElement uzemnev = driver.findElement(By.cssSelector("input[name=megbizo]"));
        WebElement keretSz = driver.findElement(By.cssSelector("input[name=keretszerzodesSzam]"));
        WebElement szallitoSz = driver.findElement(By.cssSelector("input[name=szallitoiSzam]"));
        WebElement gyarSz = driver.findElement(By.cssSelector("input[name=gyarszam]"));
        WebElement raktar = driver.findElement(By.cssSelector("input[name=raktarhelySzam]"));
        WebElement szazalek = driver.findElement(By.cssSelector("input[name=vallalasiszazalek]"));
        WebElement hozzaad = driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary"));
        List<WebElement> uzemek;

        uzemnev.click();
        uzemek = driver.findElements(By.cssSelector("mat-option"));
        uzemek.get(2).click();
        keretSz.sendKeys(keretszerzodes.toUpperCase());
        szallitoSz.sendKeys(szallitoiszam.toUpperCase());
        gyarSz.sendKeys(gyarszam.toUpperCase());
        raktar.sendKeys(raktarhely.toUpperCase());
        szazalek.sendKeys(String.valueOf(random.nextInt(200) + 1));
        if (hozzaad.isEnabled())
            hozzaad.click();

        //endregion

        if (mentes.isEnabled())
            mentes.click();
    }

    @Test
    public void UzemSzerkesztesMegse() throws FileNotFoundException, InterruptedException {
        UzemSzerkeszteseMain();

        driver.findElement(By.cssSelector("a[href*=kivitelezok]")).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));

        List<WebElement> kivitelezokList = driver.findElements(By.cssSelector("mat-cell a.btn"));
        kivitelezokList.get(kivalasztottKivitelezo).click();
        System.out.println("again here");

        gyarszamA = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (kivalasztottUzem + 1) + ") mat-cell:nth-of-type(2)")).getText();
        keretSzA = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (kivalasztottUzem + 1) + ") mat-cell:nth-of-type(3)")).getText();
        raktarA = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (kivalasztottUzem + 1) + ") mat-cell:nth-of-type(4)")).getText();
        szallitoA = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (kivalasztottUzem + 1) + ") mat-cell:nth-of-type(5)")).getText();
        szazalekA = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (kivalasztottUzem + 1) + ") mat-cell:nth-of-type(6)")).getText();

        if (!gyarszamA.equals(gyarszamB) || !keretSzA.equals(keretSzB) || !raktarA.equals(raktarB) || !szallitoA.equals(szallitoB) || !szazalekA.equals(szazalekB)) {
            Assert.fail();
        }
        System.out.println("Okay");

    }

    @Test
    public void UzemSzerkeszteseMentes() throws FileNotFoundException, InterruptedException {
        UzemSzerkeszteseMain();

        driver.findElement(By.cssSelector("m-uj-kivitelezo button.btn-primary")).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));

        List<WebElement> kivitelezokList = driver.findElements(By.cssSelector("mat-cell a.btn"));
        kivitelezokList.get(kivalasztottKivitelezo).click();
        System.out.println("again here");

        gyarszamA = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (kivalasztottUzem + 1) + ") mat-cell:nth-of-type(2)")).getText();
        keretSzA = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (kivalasztottUzem + 1) + ") mat-cell:nth-of-type(3)")).getText();
        raktarA = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (kivalasztottUzem + 1) + ") mat-cell:nth-of-type(4)")).getText();
        szallitoA = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (kivalasztottUzem + 1) + ") mat-cell:nth-of-type(5)")).getText();
        szazalekA = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (kivalasztottUzem + 1) + ") mat-cell:nth-of-type(6)")).getText();

        if (gyarszamA.equals(gyarszamB) || keretSzA.equals(keretSzB) || raktarA.equals(raktarB) || szallitoA.equals(szallitoB) || szazalekA.equals(szazalekB)) {
            Assert.fail();
        }
        System.out.println("Okay with Save");


    }

    @Test
    public void RezsianyagReszletek() throws FileNotFoundException, InterruptedException {
        RezsianyagMain();
        Random random = new Random();

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));

        List<WebElement> rezsianyag = driver.findElements(By.cssSelector("mat-cell:nth-of-type(4) a.btn"));
        if (rezsianyag.size() == 0) {
            RezsianyagHozzaadas();
        }
        int rnd = random.nextInt(rezsianyag.size());
        String rezsiNev = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (rnd + 1) + ") mat-cell:nth-of-type(1)")).getText();
        String rezsiAr = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (rnd + 1) + ") mat-cell:nth-of-type(2)")).getText();
        System.out.println(rezsiNev + " | " + rezsiAr);
        rezsianyag.get(rnd).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
        Assert.assertTrue(driver.findElement(By.cssSelector(".m-subheader__title")).getText().equals("Adatlap"));
        String reszletNev = driver.findElement(By.cssSelector(".m-portlet__body div:nth-of-type(1) span")).getText();
        String reszletAr = driver.findElement(By.cssSelector(".m-portlet__body div:nth-of-type(2) span")).getText();

        if (!rezsiNev.equals(reszletNev) || !rezsiAr.equals(reszletAr)){
            Assert.fail();
        }
        System.out.println("Everything is fine");

    }

}
