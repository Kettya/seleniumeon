package hu.upsolution.calcon.selenium.Kivitelezok;

import hu.upsolution.calcon.selenium.Aram.adat.AdatMegbizok;
import hu.upsolution.calcon.selenium.Basic;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Megbizok extends Basic {

    List<AdatMegbizok> megbizokList = new ArrayList<>();
    List<WebElement> megbizok;

    @BeforeTest
    public void SetUp() throws FileNotFoundException {
        Scanner scMegbizok = new Scanner(new File("adat/megbizok.csv"));

        while (scMegbizok.hasNextLine()) {
            String[] split = scMegbizok.nextLine().split(",");
            megbizokList.add(new AdatMegbizok(split[0], split[1], split[2], split[3]));
        }
    }

    public void MegbizokMain() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
        driver.findElement(By.cssSelector("a[href*=megbizocegek]")).click();
        Assert.assertTrue(driver.getCurrentUrl().contains("megbizocegek"));
        megbizok = driver.findElements(By.cssSelector("mat-row mat-cell:nth-of-type(1)"));
    }

    @Test
    public void MegbizoHozzaadas() throws FileNotFoundException, InterruptedException {
        MegbizokMain();

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
        driver.findElement(By.cssSelector("m-subheader a.btn")).click();
        Assert.assertTrue(driver.getCurrentUrl().contains("/uj"));

        WebElement rovidNev = driver.findElement(By.cssSelector("input[name=megbizoRovidNev]"));
        WebElement hosszuNev = driver.findElement(By.cssSelector("input[name=megbizoTeljesNev]"));
        WebElement telepules = driver.findElement(By.cssSelector("mat-select[name=telepules]"));
        WebElement vallalat = driver.findElement(By.cssSelector("mat-select[name=vallalat]"));
        WebElement mentes = driver.findElement(By.cssSelector("m-uj-megbizo-ceg button.btn-primary"));
        WebElement fejlec = driver.findElement(By.cssSelector("m-subheader h3"));
        System.out.println(fejlec.getText());
        Assert.assertTrue(fejlec.getText().equals("Új megbízó cég"));

        int megbizo = 0;

        rovidNev.sendKeys(megbizokList.get(megbizo).getRovidNev());
        hosszuNev.sendKeys(megbizokList.get(megbizo).getHosszuNev());

        telepules.click();
        List<WebElement> telepulesList = driver.findElements(By.cssSelector("mat-option"));
        for (int i = 0; i < telepulesList.size(); i++) {
            if (telepulesList.get(i).getText().equals(megbizokList.get(megbizo).getVaros())) {
                telepulesList.get(i).click();
                break;
            }
        }

        vallalat.click();
        List<WebElement> vallalatList = driver.findElements(By.cssSelector("mat-option"));
        for (int i = 0; i < vallalatList.size(); i++) {
            if (vallalatList.get(i).getText().contains(megbizokList.get(megbizo).getVallalat())) {
                vallalatList.get(i).click();
                break;
            }
        }

        if (mentes.isEnabled())
            mentes.click();

//        System.out.println(megbizok.size());
//        for (int i = 0; i < megbizok.size(); i++) {
//            System.out.println(megbizok.get(i).getText());
//        }

    }

    @Test
    public void MegbizoSzerkesztese() throws FileNotFoundException, InterruptedException {
        MegbizokMain();

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
        List<WebElement> megbizok = driver.findElements(By.cssSelector("mat-cell a.btn"));
        int a = 3;
        megbizok.get(a).click();
        Assert.assertTrue(driver.findElement(By.cssSelector("m-subheader h3")).getText().equals("Üzem szerkesztése"));
        driver.findElement(By.cssSelector("input[name=megbizoRovidNev]")).sendKeys(" Sz");
        driver.findElement(By.cssSelector("input[name=megbizoTeljesNev]")).sendKeys(" Szerkesztett");
        driver.findElement(By.cssSelector("m-uj-megbizo-ceg button.btn-primary")).click();

    }

    @Test
    public void MegbizoKereses() throws FileNotFoundException, InterruptedException {
        MegbizokMain();

        Random random = new Random();

        WebElement rovid = driver.findElement(By.cssSelector("input[name=megbizoRovidNev]"));
        WebElement teljes = driver.findElement(By.cssSelector("input[name=megbizoTeljesNev]"));
        WebElement torles = driver.findElement(By.cssSelector("span i.material-icons"));
        List<WebElement> eredmeny;
        int results = 0;
        while (results == 0) {
            int s = random.nextInt(2);
            int abc = random.nextInt(megbizokList.size());
            if (s == 0) {
                rovid.sendKeys(megbizokList.get(abc).getRovidNev(), Keys.ENTER);
                wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
                eredmeny = driver.findElements(By.cssSelector("mat-row"));
                System.out.println("Results: " + eredmeny.size());
                if (eredmeny.size() > 0) {
                    results = eredmeny.size();
                    break;
                }
                torles.click();
            }
            if (s == 1) {
                teljes.sendKeys(megbizokList.get(abc).getHosszuNev(), Keys.ENTER);
                wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
                eredmeny = driver.findElements(By.cssSelector("mat-row"));
                System.out.println("Results: " + eredmeny.size());
                if (eredmeny.size() > 0) {
                    results = eredmeny.size();
                    break;
                }
                torles.click();
            }
        }


    }
}
