package hu.upsolution.calcon.selenium.Anyag;

import hu.upsolution.calcon.selenium.Basic;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;

public class Anyag extends Basic {

    private String elsoFile = "Anyagok ellenőrző fájl-SPS-2018.10.17.xlsx";
    private String secondFile = "Anyagok ellenőrző fájl-SPS-2019.02.22 kozep.xlsx";
    private String thirdFile = "Anyagok ellenőrző fájl-SPS-2019.02.22 third.xlsx";
    private String invalidFajl = "New Text Documnet.txt";
    private String invalidXls = "Untitled spreadsheet.xlsx";
    private String abc = "aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyz0123456789";
    private Random random = new Random();
    private Path currentPath = Paths.get("");
    private String s = currentPath.toAbsolutePath().toString() + "\\adat\\anyagok\\";
    private boolean isFirst = false;
    private boolean varakozo = false;
    private int m;
    private int c;
    private int p;
    private int a;
    private int b;


    private void AnyagMain() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
        driver.findElement(By.cssSelector("a[href*=anyagok]")).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));

        System.out.println(isFirst);
        System.out.println(varakozo);
        List<WebElement> anyagok = driver.findElements(By.cssSelector("mat-row"));
        //   if (driver.findElement(By.cssSelector(".norecordfound")).isDisplayed()) {
        if (anyagok.size() == 0) {
            isFirst = true;
        }
        System.out.println(isFirst);

        if (anyagok.size() != 0) {
            for (WebElement element : anyagok) {
                if (element.findElement(By.cssSelector("mat-cell:nth-of-type(3)")).getText().equals("Várakozó")) {
                    varakozo = true;
                    break;
                }
            }
        }
        System.out.println(varakozo);
    }

    private void AnyagHozzaadasMain() throws FileNotFoundException, InterruptedException {
        AnyagMain();
        driver.findElement(By.cssSelector("m-subheader a.btn")).click();
        Varakozo();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));

        Assert.assertEquals(driver.findElement(By.cssSelector(".m-subheader__title")).getText(), "Új anyag lista");
    }

    private void Wait() {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
    }

    private void EgyediAras() {

        if (isFirst) {
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
            Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed());
            driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary")).click();
            Assert.fail();
        }
    }

    private void Varakozo() {
        if (varakozo) {
            Wait();
            Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed());
            driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary")).click();
            Wait();
        }
    }

    private void Aktivalas() {
        Wait();
        Assert.assertTrue(driver.findElement(By.cssSelector("m-anyaglistaaktivalas")).isDisplayed());
        driver.findElement(By.cssSelector("m-anyaglistaaktivalas button.btn-primary:nth-of-type(2)")).click();
    }

    private void VarakozoAnyaglist() {
        List<WebElement> fileList = driver.findElements(By.cssSelector("mat-cell a.btn"));

        for (WebElement element : fileList) {
            if (element.getText().equals("Validálás")) {
                element.click();
                break;
            }
        }
    }

    private void AktualisAnyaglist() {
        List<WebElement> fileList = driver.findElements(By.cssSelector("mat-cell a.btn"));

        for (WebElement element : fileList) {
            if (element.getText().equals("Részletek")) {
                element.click();
                break;
            }
        }
    }

    public void Something() {
        String max = driver.findElement(By.cssSelector(".mat-paginator-range-label")).getText();
        String select = driver.findElement(By.cssSelector(".mat-select-value")).getText();
        String[] split = max.split(" ");
        m = Integer.parseInt(split[4]);
        c = Integer.parseInt(split[2]);
        p = Integer.parseInt(select);
        a = m / c;
        b = a * c;

        System.out.println(m);
        System.out.println(a);
        System.out.println(s);

    }

    public void Aktivalando() {
        if (driver.findElement(By.cssSelector("m-anyaglistaaktivalas")).isDisplayed()) {
            System.out.println("Akltiválható");
            Aktivalas();
        }
    }

    @Test
    public void AnyagReszletekMain() throws FileNotFoundException, InterruptedException {
        AnyagMain();

        driver.findElement(By.cssSelector("mat-cell a.btn")).click();
        String max = driver.findElement(By.cssSelector(".mat-paginator-range-label")).getText();
        String select = driver.findElement(By.cssSelector(".mat-select-value")).getText();
        String[] split = max.split(" ");
        m = Integer.parseInt(split[4]);
        c = Integer.parseInt(split[2]);
        p = Integer.parseInt(select);
        a = m / c;
        b = a * c;
        int rnd = random.nextInt(a);
        System.out.println(split[2]);
        System.out.println(split[4]);
        System.out.println(select);
        System.out.println(a);
        System.out.println(b);
        System.out.println(rnd);

        WebElement next = driver.findElement(By.cssSelector(".mat-paginator-navigation-next"));
        for (int i = 0; i < a; i++) {
            next.click();
        }


    }

    @Test
    public void AnyagFeltoltesFirst() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        AnyagHozzaadasMain();
        System.out.println(s);
        WebElement ujanyag = driver.findElement(By.cssSelector("input[type=file]"));
        WebElement mentes = driver.findElement(By.cssSelector("m-ujanyag button.btn-primary"));
        Assert.assertFalse(mentes.isEnabled());
        ujanyag.sendKeys(s + elsoFile);

        String uploaded = driver.findElement(By.cssSelector("m-ujanyag .mat-list-item")).getText().replace("close", "").trim();
        System.out.println(uploaded);
        Assert.assertEquals(uploaded, elsoFile);
        Assert.assertTrue(mentes.isEnabled());
        mentes.click();

    }

    @Test
    public void AnyagFeltoltesSecond() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        AnyagHozzaadasMain();
        System.out.println(s);
        WebElement ujanyag = driver.findElement(By.cssSelector("input[type=file]"));
        WebElement mentes = driver.findElement(By.cssSelector("m-ujanyag button.btn-primary"));
        Assert.assertFalse(mentes.isEnabled());
        ujanyag.sendKeys(s + secondFile);

        String uploaded = driver.findElement(By.cssSelector("m-ujanyag .mat-list-item")).getText().replace("close", "").trim();
        System.out.println(uploaded);
        Assert.assertEquals(uploaded, secondFile);
        Assert.assertTrue(mentes.isEnabled());
        mentes.click();

        EgyediAras();
    }

    @Test
    public void AnyagFeltoltesSecondEsAktivalas() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        AnyagHozzaadasMain();
        System.out.println(s);
        WebElement ujanyag = driver.findElement(By.cssSelector("input[type=file]"));
        WebElement mentes = driver.findElement(By.cssSelector("m-ujanyag button.btn-primary"));
        Assert.assertFalse(mentes.isEnabled());
        ujanyag.sendKeys(s + secondFile);

        String uploaded = driver.findElement(By.cssSelector("m-ujanyag .mat-list-item")).getText().replace("close", "").trim();
        System.out.println(uploaded);
        Assert.assertEquals(uploaded, secondFile);
        Assert.assertTrue(mentes.isEnabled());
        mentes.click();

        EgyediAras();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("m-anyaglistaaktivalas")));
        Aktivalando();
    }

    @Test
    public void AnyagFeltoltesThird() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        AnyagHozzaadasMain();
        System.out.println(s);
        WebElement ujanyag = driver.findElement(By.cssSelector("input[type=file]"));
        WebElement mentes = driver.findElement(By.cssSelector("m-ujanyag button.btn-primary"));
        Assert.assertFalse(mentes.isEnabled());
        ujanyag.sendKeys(s + thirdFile);

        String uploaded = driver.findElement(By.cssSelector("m-ujanyag .mat-list-item")).getText().replace("close", "").trim();
        System.out.println(uploaded);
        Assert.assertEquals(uploaded, thirdFile);
        Assert.assertTrue(mentes.isEnabled());
        mentes.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
        Wait();
        Aktivalando();
        EgyediAras();
    }

    @Test
    public void AnyagFeltoltesThirdEsAktivalas() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        AnyagHozzaadasMain();
        System.out.println(s);
        WebElement ujanyag = driver.findElement(By.cssSelector("input[type=file]"));
        WebElement mentes = driver.findElement(By.cssSelector("m-ujanyag button.btn-primary"));
        Assert.assertFalse(mentes.isEnabled());
        ujanyag.sendKeys(s + thirdFile);

        String uploaded = driver.findElement(By.cssSelector("m-ujanyag .mat-list-item")).getText().replace("close", "").trim();
        System.out.println(uploaded);
        Assert.assertEquals(uploaded, thirdFile);
        Assert.assertTrue(mentes.isEnabled());
        mentes.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
        Wait();
        Aktivalando();
        EgyediAras();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("m-anyaglistaaktivalas")));
        Aktivalando();
    }

    @Test
    public void AnyagReszlet() throws FileNotFoundException, InterruptedException {
        AnyagReszletekMain();

        List<WebElement> reszletek = driver.findElements(By.cssSelector("mat-cell a.btn"));
        int rnd = random.nextInt(reszletek.size());
        String anyagNev = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (rnd + 1) + ") mat-cell:nth-of-type(1)")).getText();
        String anyagAr = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (rnd + 1) + ") mat-cell:nth-of-type(2)")).getText();
        System.out.println(anyagNev + " | " + anyagAr);
//        reszletek.get(rnd).click();
//        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
//        Assert.assertTrue(driver.findElement(By.cssSelector(".m-subheader__title")).getText().equals("Adatlap"));
//        String reszletNev = driver.findElement(By.cssSelector(".m-portlet__body div:nth-of-type(1) span")).getText();
//        String reszletAr = driver.findElement(By.cssSelector(".m-portlet__body div:nth-of-type(2) span")).getText();
//
//        if (!anyagNev.equals(reszletNev) || !anyagAr.equals(reszletAr)) {
//            Assert.fail();
//        }
//        System.out.println("Everything is fine");

    }

    @Test
    public void Validalas() throws FileNotFoundException, InterruptedException {
        AnyagMain();

        VarakozoAnyaglist();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));

        Assert.assertTrue(driver.getCurrentUrl().contains("fuggotetelek"));
        Assert.assertEquals(driver.findElement(By.cssSelector(".m-subheader__title")).getText(), "Függőben lévő anyagok");


        WebElement next = driver.findElement(By.cssSelector(".mat-paginator-navigation-next"));
//
        for (int j = 0; j < a; j++) {
            List<WebElement> fuggoAnyag = driver.findElements(By.cssSelector("mat-cell a.btn"));
            for (WebElement element : fuggoAnyag) {
                int rnd = random.nextInt(999) + 1000;
                int csere = random.nextInt(2);
                String fuggo = element.getText();
                //            fuggoAnyag.get(0).click();
                driver.findElement(By.cssSelector("mat-row:nth-of-type(1) mat-cell a.btn")).click();
                Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed());
                if (fuggo.contains("jóváhagyás")) {
                    System.out.println("ASSAAA");
                    driver.findElement(By.cssSelector("mat-dialog-container input")).sendKeys(String.valueOf(rnd));
                    driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary")).click();
                }

                if (fuggo.equals("Cserél")) {
                    if (csere == 0) {
                        System.out.println("Csere");
                        driver.findElement(By.cssSelector("input[name=anyagId]")).sendKeys(String.valueOf(abc.charAt(random.nextInt(abc.length()))));
                        List<WebElement> csereAnyag = driver.findElements(By.cssSelector("mat-option"));
                        csereAnyag.get(random.nextInt(csereAnyag.size())).click();
                        driver.findElement(By.cssSelector(".mat-dialog-actions button.btn-primary")).click();
                    }
                    if (csere == 1) {
                        System.out.println("Inaktiválás");
                        driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary:nth-of-type(1)")).click();
                    }

                }

            }
            next.click();
        }
//        Wait();
//        Assert.assertTrue(driver.findElement(By.cssSelector("m-anyaglistaaktivalas")).isDisplayed());
//        Aktivalas();

    }

    @Test
    public void AnyagAktivalas() throws FileNotFoundException, InterruptedException {
        AnyagMain();


        List<WebElement> fileList = driver.findElements(By.cssSelector("mat-cell a.btn"));
        Assert.assertTrue(fileList.size() > 1);

        for (WebElement element : fileList) {
            if (element.getText().equals("Aktiválás")) {
                element.click();
                break;
            }
        }
        Wait();
        Assert.assertTrue(driver.getCurrentUrl().contains("fuggotetelek"));
        Aktivalas();
        Wait();

        Assert.assertTrue(driver.getCurrentUrl().contains("anyagok"));
        List<WebElement> aktivaltAnyagok = driver.findElements(By.cssSelector("mat-row"));
        Assert.assertEquals(aktivaltAnyagok.size(), 1);

    }

    @Test
    public void ReszlegesAktivalas() throws FileNotFoundException, InterruptedException {
        AnyagMain();
        VarakozoAnyaglist();
    }

    @Test
    public void VarakozoAnyagKeresesMain() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        AnyagMain();
        VarakozoAnyaglist();
    }// main

    @Test
    public void AktualisAnyagKeresesMain() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        AnyagMain();
        AktualisAnyaglist();

    } // main

    @Test
    private void InvalidFajl() throws FileNotFoundException, InterruptedException {
        AnyagHozzaadasMain();

        System.out.println(s);
        WebElement ujanyag = driver.findElement(By.cssSelector("input[type=file]"));
        WebElement mentes = driver.findElement(By.cssSelector("m-ujanyag button.btn-primary"));
        Assert.assertFalse(mentes.isEnabled());
        ujanyag.sendKeys(s + invalidFajl);

        String uploaded = driver.findElement(By.cssSelector("m-ujanyag .mat-list-item")).getText().replace("close", "").trim();
        System.out.println(uploaded);
        Assert.assertEquals(uploaded, invalidFajl);
        Assert.assertTrue(mentes.isEnabled());
        mentes.click();

        Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed());

    }

    @Test
    private void InvalidXlsx() throws FileNotFoundException, InterruptedException {
        AnyagHozzaadasMain();

        System.out.println(s);
        WebElement ujanyag = driver.findElement(By.cssSelector("input[type=file]"));
        WebElement mentes = driver.findElement(By.cssSelector("m-ujanyag button.btn-primary"));
        Assert.assertFalse(mentes.isEnabled());
        ujanyag.sendKeys(s + invalidXls);

        String uploaded = driver.findElement(By.cssSelector("m-ujanyag .mat-list-item")).getText().replace("close", "").trim();
        System.out.println(uploaded);
        Assert.assertEquals(uploaded, invalidXls);
        Assert.assertTrue(mentes.isEnabled());
        mentes.click();

        Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed());
    }
}
