package hu.upsolution.calcon.selenium.Anyag;

import hu.upsolution.calcon.selenium.Basic;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;

public class Valtozo extends Basic {

    private String elsoFile = "vtetel.xlsx";
    private String secondFile = "vtetel2.xlsx";
    private String invalidFajl = "New Text Document.txt";
    private String invalidXls = "Untitled spreadsheet.xlsx";
    private String abc = "aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyz0123456789";
    private Random random = new Random();
    private Path currentPath = Paths.get("");
    private String s = currentPath.toAbsolutePath().toString() + "\\adat\\valtozo\\";
    private boolean isFirst = false;
    private boolean varakozo = false;
    private int m;
    private int c;
    private int p;
    private int a;
    private int b;

    private void Wait() {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
    }

    private void Varakozo() {
        if (varakozo) {
            Wait();
            Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed());
            driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary")).click();
            Wait();
        }
    }

    private void VarakozoAnyaglist() {
        List<WebElement> fileList = driver.findElements(By.cssSelector("mat-cell a.btn"));

        for (WebElement element : fileList) {
            if (element.getText().equals("Validálás")) {
                element.click();
                break;
            }
        }
    }

    private void AktualisAnyaglist() {
        List<WebElement> fileList = driver.findElements(By.cssSelector("mat-cell a.btn"));

        for (WebElement element : fileList) {
            if (element.getText().equals("Részletek")) {
                element.click();
                break;
            }
        }
    }

    public void Something() {
        String max = driver.findElement(By.cssSelector(".mat-paginator-range-label")).getText();
        String select = driver.findElement(By.cssSelector(".mat-select-value")).getText();
        String[] split = max.split(" ");
        m = Integer.parseInt(split[4]);
        c = Integer.parseInt(split[2]);
        p = Integer.parseInt(select);
        a = m / c;
        b = a * c;

        System.out.println(m);
        System.out.println(a);
        System.out.println(s);

    }

    @Test
    public void ValtozoReszletekMain() throws FileNotFoundException, InterruptedException {
        ValtozoMain();

        driver.findElement(By.cssSelector("mat-cell a.btn")).click();
        String max = driver.findElement(By.cssSelector(".mat-paginator-range-label")).getText();
        String select = driver.findElement(By.cssSelector(".mat-select-value")).getText();
        String[] split = max.split(" ");
        m = Integer.parseInt(split[4]);
        c = Integer.parseInt(split[2]);
        p = Integer.parseInt(select);
        a = m / c;
        b = a * c;
        int rnd = random.nextInt(a);
        System.out.println(split[2]);
        System.out.println(split[4]);
        System.out.println(select);
        System.out.println(a);
        System.out.println(b);
        System.out.println(rnd);

        WebElement next = driver.findElement(By.cssSelector(".mat-paginator-navigation-next"));
        for (int i = 0; i < a; i++) {
            next.click();
        }


    } //lapozó

    @Test
    public void ValtozoReszlet() throws FileNotFoundException, InterruptedException {
        ValtozoReszletekMain();

        List<WebElement> reszletek = driver.findElements(By.cssSelector("mat-cell a.btn"));
        int rnd = random.nextInt(reszletek.size());
        String anyagNev = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (rnd + 1) + ") mat-cell:nth-of-type(1)")).getText();
        String anyagAr = driver.findElement(By.cssSelector("mat-row:nth-of-type(" + (rnd + 1) + ") mat-cell:nth-of-type(2)")).getText();
        System.out.println(anyagNev + " | " + anyagAr);
        reszletek.get(rnd).click();
//        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
//        Assert.assertTrue(driver.findElement(By.cssSelector(".m-subheader__title")).getText().equals("Adatlap"));
//        String reszletNev = driver.findElement(By.cssSelector(".m-portlet__body div:nth-of-type(1) span")).getText();
//        String reszletAr = driver.findElement(By.cssSelector(".m-portlet__body div:nth-of-type(2) span")).getText();
//
//        if (!anyagNev.equals(reszletNev) || !anyagAr.equals(reszletAr)) {
//            Assert.fail();
//        }
//        System.out.println("Everything is fine");

    }

    @Test
    private void ValtozoMain() throws FileNotFoundException, InterruptedException {
        Bejelentkezes();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
        driver.findElement(By.cssSelector("a[href*=valtozoanyagok]")).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));

        System.out.println(isFirst);
        System.out.println(varakozo);
        List<WebElement> valtozotetel = driver.findElements(By.cssSelector("mat-row"));
        //   if (driver.findElement(By.cssSelector(".norecordfound")).isDisplayed()) {
        if (valtozotetel.size() == 0) {
            isFirst = true;
        }
        System.out.println(isFirst);

        if (valtozotetel.size() != 0) {
            for (WebElement element : valtozotetel) {
                if (element.findElement(By.cssSelector("mat-cell:nth-of-type(3)")).getText().equals("Várakozó")) {
                    varakozo = true;
                    break;
                }
            }
        }
        System.out.println(varakozo);
    }

    @Test
    private void ValtozotetelHozzaadasMain() throws FileNotFoundException, InterruptedException {
        ValtozoMain();
        driver.findElement(By.cssSelector("m-subheader a.btn")).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));
        Varakozo();

        Assert.assertEquals(driver.findElement(By.cssSelector(".m-subheader__title")).getText(), "Új anyag lista");

    }

    @Test
    private void ValtozoTetelFirst() throws FileNotFoundException, InterruptedException {
        ValtozotetelHozzaadasMain();


        System.out.println(s);
        WebElement ujanyag = driver.findElement(By.cssSelector("input[type=file]"));
        WebElement mentes = driver.findElement(By.cssSelector("m-uj-valtozo-anyag button.btn-primary"));
        Assert.assertFalse(mentes.isEnabled());
        ujanyag.sendKeys(s + elsoFile);

        String uploaded = driver.findElement(By.cssSelector("m-uj-valtozo-anyag .mat-list-item")).getText().replace("close", "").trim();
        System.out.println(uploaded);
        Assert.assertEquals(uploaded, elsoFile);
        Assert.assertTrue(mentes.isEnabled());
        mentes.click();
    }

    @Test
    private void ValtozoTetelSecond() throws FileNotFoundException, InterruptedException {
        ValtozotetelHozzaadasMain();

        System.out.println(s);
        WebElement ujanyag = driver.findElement(By.cssSelector("input[type=file]"));
        WebElement mentes = driver.findElement(By.cssSelector("m-uj-valtozo-anyag button.btn-primary"));
        Assert.assertFalse(mentes.isEnabled());
        ujanyag.sendKeys(s + secondFile);

        String uploaded = driver.findElement(By.cssSelector("m-uj-valtozo-anyag .mat-list-item")).getText().replace("close", "").trim();
        System.out.println(uploaded);
        Assert.assertEquals(uploaded, secondFile);
        Assert.assertTrue(mentes.isEnabled());
        mentes.click();
    }

    @Test
    private void InvalidFajl() throws FileNotFoundException, InterruptedException {
        ValtozotetelHozzaadasMain();

        System.out.println(s);
        WebElement ujanyag = driver.findElement(By.cssSelector("input[type=file]"));
        WebElement mentes = driver.findElement(By.cssSelector("m-uj-valtozo-anyag button.btn-primary"));
        Assert.assertFalse(mentes.isEnabled());
        ujanyag.sendKeys(s + invalidFajl);

        String uploaded = driver.findElement(By.cssSelector("m-uj-valtozo-anyag .mat-list-item")).getText().replace("close", "").trim();
        System.out.println(uploaded);
        Assert.assertEquals(uploaded, invalidFajl);
        Assert.assertTrue(mentes.isEnabled());
        mentes.click();

        Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed());

    }

    @Test
    private void InvalidXlsx() throws FileNotFoundException, InterruptedException {
        ValtozotetelHozzaadasMain();

        ValtozotetelHozzaadasMain();

        System.out.println(s);
        WebElement ujanyag = driver.findElement(By.cssSelector("input[type=file]"));
        WebElement mentes = driver.findElement(By.cssSelector("m-uj-valtozo-anyag button.btn-primary"));
        Assert.assertFalse(mentes.isEnabled());
        ujanyag.sendKeys(s + invalidXls);

        String uploaded = driver.findElement(By.cssSelector("m-uj-valtozo-anyag .mat-list-item")).getText().replace("close", "").trim();
        System.out.println(uploaded);
        Assert.assertEquals(uploaded, invalidXls);
        Assert.assertTrue(mentes.isEnabled());
        mentes.click();

        Assert.assertTrue(driver.findElement(By.cssSelector("mat-dialog-container")).isDisplayed());
    }

    @Test
    public void Validalas() throws FileNotFoundException, InterruptedException {
        ValtozoMain();
        VarakozoAnyaglist();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("m-splash screen")));

        Assert.assertTrue(driver.getCurrentUrl().contains("fuggotetelek"));
        Assert.assertEquals(driver.findElement(By.cssSelector(".m-subheader__title")).getText(), "Függőben lévő anyagok");


        WebElement next = driver.findElement(By.cssSelector(".mat-paginator-navigation-next"));
//
        for (int j = 0; j < a; j++) {
            List<WebElement> fuggoAnyag = driver.findElements(By.cssSelector("mat-cell a.btn"));
            for (WebElement element : fuggoAnyag) {
                int csere = random.nextInt(2);
                String fuggo = element.getText();
                if (fuggo.equals("Cserél")) {
                    if (csere == 0) {
                        System.out.println("Csere");
                        driver.findElement(By.cssSelector("input[name=anyagId]")).sendKeys(String.valueOf(abc.charAt(random.nextInt(abc.length()))));
                        List<WebElement> csereAnyag = driver.findElements(By.cssSelector("mat-option"));
                        csereAnyag.get(random.nextInt(csereAnyag.size())).click();
                        driver.findElement(By.cssSelector(".mat-dialog-actions button.btn-primary")).click();
                    }
                    if (csere == 1) {
                        System.out.println("Inaktiválás");
                        driver.findElement(By.cssSelector("mat-dialog-container button.btn-primary:nth-of-type(1)")).click();
                    }

                }

            }
            next.click();
        }
//        Wait();
//        Assert.assertTrue(driver.findElement(By.cssSelector("m-anyaglistaaktivalas")).isDisplayed());
//        Aktivalas();

    }

}
